﻿using System.Drawing;
using System.Windows.Forms;

namespace prounion.Controls
{
    public sealed class DialogHeader : Control
    {
        private readonly float _borderWith;
        private readonly float _padding;
        private readonly float _textPadding;
        private readonly float _imageTextPadding;
        private readonly Font _textFont;
        private readonly Font _descFont;
        private readonly Color _colBorderColor;
        private readonly Color _colBackColor;
        private readonly Color _colForeColorText;
        private readonly Color _colForeColorDesc;

        public string Description { get; set; }
        public Image TextImage { get; set; }

        public DialogHeader()
        {
            _borderWith = 1;
            _padding = 10;
            _textPadding = -5;
            _imageTextPadding = 20;

            _colBackColor = Color.White;
            _colBorderColor = Color.FromArgb(175, 175, 175);
            _colForeColorText = Color.FromArgb(0, 51, 153);
            _colForeColorDesc = Color.FromArgb(149, 149, 149);

            Height = 50;
            Margin = new Padding(0);
            Dock = DockStyle.Top;

            _textFont = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            _descFont = new Font("Segoe UI", 8.0F, FontStyle.Regular, GraphicsUnit.Point, 0);

            SetStyle(ControlStyles.Selectable, false);
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw | ControlStyles.UserPaint, true);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            var brushText = new SolidBrush(_colForeColorText);
            var brushDesc = new SolidBrush(_colForeColorDesc);

            var imageWidth = (TextImage != null) ? TextImage.Width : 0;
            var graphics = e.Graphics;

            var szText = graphics.MeasureString(Text, _textFont);
            var szDesc = graphics.MeasureString(Description, _descFont);

            var textHeight = szText.Height + szDesc.Height + _textPadding;
            var paddingTop = (Height - textHeight) / 2;

            if (TextImage != null)
            {
                var b = (Bitmap)TextImage;
                graphics.DrawImage(b, _padding, (Height - b.Height) / 2, b.Width, b.Height);
            }

            var pen = new Pen(_colBorderColor) { Width = _borderWith };

            graphics.DrawLine(pen, new Point(0, Height - 2), new Point(Width - 2, Height - 2));

            graphics.DrawString(Text, _textFont, brushText, _imageTextPadding + imageWidth, paddingTop);
            graphics.DrawString(Description, _descFont, brushDesc, _imageTextPadding + imageWidth, paddingTop + szText.Height + _textPadding);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            base.OnPaintBackground(pevent);

            BackColor = _colBackColor;
        }

        protected override Size DefaultSize
        {
            get { return new Size(408, 60); }
        }
    }
}