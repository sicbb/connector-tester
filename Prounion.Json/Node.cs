﻿using System.Globalization;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace Prounion.Json
{
    public class Node
    {
        public TreeNode FromJson(string name, object obj)
        {
            var node = new TreeNode(name);

            if (obj == null)
            {
                node.Tag = new JValue("1");
                return node;
            }

            switch (obj.GetType().Name)
            {
                case "JValue":
                    var jValue = (JValue) obj;
                    node.Text += jValue.Parent.GetType().Name == "JArray" ? jValue.ToString(CultureInfo.InvariantCulture) : ": " + jValue;
                    node.Tag = jValue;
                    break;
                case "JArray":
                    node.Text = name + "[]";
                    node.Tag = obj;
                    foreach (var array in (JArray)obj)
                    {
                        var tmpName = array.GetType().Name == "JObject" ? "object" : string.Empty;
                        node.Nodes.Add(FromJson(tmpName, array));
                    }
                    
                    break;
                case "JObject":
                    node.Text = name;
                    node.Tag = obj;
                    foreach (var x in (JObject)obj)
                    {
                        node.Nodes.Add(FromJson(x.Key, x.Value));
                    }
                    break;
            }

            return node;
        }
    }
}