﻿using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ConnectorIdentification : MainModel
    {
        [JsonProperty(Required = Required.AllowNull, PropertyName = "endpointVersion")]
        public string EndpointVersion { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "platformName")]
        public string PlatformName { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "platformVersion")]
        public string PlatformVersion { get; set; }
    }
}
