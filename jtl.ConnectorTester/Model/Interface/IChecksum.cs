﻿namespace jtl.ConnectorTester.Model.Interface
{
    public interface IChecksum
    {
        Identity ForeignKey { get; set; }
        string Endpoint { get; set; }
        string Host { get; set; }
        int Type { get; set; }
        bool HasChanged { get; set; }
    }
}
