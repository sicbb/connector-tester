using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class Product2CategoryModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "categoryId")]
		public Identity CategoryId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }
	}
}
