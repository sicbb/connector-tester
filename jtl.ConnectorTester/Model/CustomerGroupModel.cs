using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CustomerGroupModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "applyNetPrice")]
		public Boolean ApplyNetPrice { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "discount")]
		public Double Discount { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isDefault")]
		public Boolean IsDefault { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "attributes")]
		public List<CustomerGroupAttrModel> Attributes { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<CustomerGroupI18nModel> I18ns { get; set; }
	}
}
