using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class FileDownloadHistoryModel : MainModel
    {
        [JsonProperty(Required = Required.Always, PropertyName = "id")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity Id { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "fileDownloadId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity FileDownloadId { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "customerId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity CustomerId { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "customerOrderId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity CustomerOrderId { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "creationDate")]
        public DateTime? CreationDate { get; set; }

    }
}