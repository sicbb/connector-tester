using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CategoryFunctionAttrModel : MainModel
    {
        [JsonProperty(Required = Required.Always, PropertyName = "id")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity Id { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "categoryId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity CategoryId { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "value")]
        public string Value { get; set; }

    }
}