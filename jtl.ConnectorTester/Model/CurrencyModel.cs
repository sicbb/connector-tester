using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CurrencyModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "delimiterCent")]
		public String DelimiterCent { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "delimiterThousand")]
		public String DelimiterThousand { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "factor")]
		public Double Factor { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "hasCurrencySignBeforeValue")]
		public Boolean HasCurrencySignBeforeValue { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isDefault")]
		public Boolean IsDefault { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "iso")]
		public String Iso { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "name")]
		public String Name { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "nameHtml")]
		public String NameHtml { get; set; }
	}
}
