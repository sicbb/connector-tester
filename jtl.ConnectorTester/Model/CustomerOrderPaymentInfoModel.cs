using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CustomerOrderPaymentInfoModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerOrderId")]
		public Identity CustomerOrderId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "accountHolder")]
		public String AccountHolder { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "accountNumber")]
		public String AccountNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "bankCode")]
		public String BankCode { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "bankName")]
		public String BankName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "bic")]
		public String Bic { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creditCardExpiration")]
		public String CreditCardExpiration { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creditCardHolder")]
		public String CreditCardHolder { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creditCardNumber")]
		public String CreditCardNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creditCardType")]
		public String CreditCardType { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creditCardVerificationNumber")]
		public String CreditCardVerificationNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "iban")]
		public String Iban { get; set; }
	}
}
