using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ChecksumModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "foreignKey")]
		public Identity ForeignKey { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "endpoint")]
		public String Endpoint { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "hasChanged")]
		public Boolean HasChanged { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "host")]
		public String Host { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "type")]
		public string Type { get; set; }
	}
}
