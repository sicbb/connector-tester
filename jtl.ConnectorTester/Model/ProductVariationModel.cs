using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductVariationModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sort")]
		public Int32 Sort { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "type")]
		public String Type { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<ProductVariationI18nModel> I18ns { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "invisibilities")]
		public List<ProductVariationInvisibilityModel> Invisibilities { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "values")]
		public List<ProductVariationValueModel> Values { get; set; }
	}
}
