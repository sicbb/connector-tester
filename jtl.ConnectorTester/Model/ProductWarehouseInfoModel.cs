using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductWarehouseInfoModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "warehouseId")]
		public Identity warehouseId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "inflowQuantity")]
		public Double InflowQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "stockLevel")]
		public Double stockLevel { get; set; }
	}
}
