using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class PaymentMethodModel : MainModel
    {
        [JsonProperty(Required = Required.Always, PropertyName = "id")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity Id { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "sort")]
        public int Sort { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "moduleId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity ModuleId { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "picture")]
        public string Picture { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "vendor")]
        public string Vendor { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "useMail")]
        public bool UseMail { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "isActive")]
        public bool IsActive { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "isUseable")]
        public bool IsUseable { get; set; }

    }
}