using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductVariationValueModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productVariationId")]
		public Identity ProductVariationId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "ean")]
		public String Ean { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "extraWeight")]
		public Double ExtraWeight { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sku")]
		public String Sku { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sort")]
		public Int32 Sort { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "stockLevel")]
		public Double StockLevel { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "extraCharges")]
		public List<ProductVariationValueExtraChargeModel> ExtraCharges { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<ProductVariationValueI18nModel> I18ns { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "invisibilities")]
		public List<ProductVariationValueInvisibilityModel> Invisibilities { get; set; }
	}
}
