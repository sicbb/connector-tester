using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class SpecialPriceModel : MainModel
    {
        [JsonProperty(Required = Required.Always, PropertyName = "customerGroupId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity CustomerGroupId { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "productSpecialPriceId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity ProductSpecialPriceId { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "priceNet")]
        public double PriceNet { get; set; }

    }
}