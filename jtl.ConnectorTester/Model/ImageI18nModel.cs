﻿using System;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ImageI18nModel : MainModel
    {
        [JsonConverter(typeof(IdentityConverter))]
        [JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
        public Identity Id { get; set; }

        [JsonConverter(typeof (IdentityConverter))]
        [JsonProperty(Required = Required.AllowNull, PropertyName = "imageId")]
        public Identity ImageId { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "languageISO")]
		public String LanguageISO { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "altText")]
		public String AltText { get; set; }
    }
}
