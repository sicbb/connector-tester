using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ConnectorServerInfoModel : MainModel
    {
		[JsonProperty(Required = Required.AllowNull, PropertyName = "executionTime")]
		public Int32 ExecutionTime { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "memoryLimit")]
		public Int32 MemoryLimit { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "postMaxSize")]
		public Int32 PostMaxSize { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "uploadMaxFilesize")]
		public Int32 UploadMaxFilesize { get; set; }
	}
}
