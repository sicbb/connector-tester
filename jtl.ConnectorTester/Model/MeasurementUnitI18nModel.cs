using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class MeasurementUnitI18nModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "measurementUnitId")]
		public Identity MeasurementUnitId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "languageISO")]
		public String LanguageISO { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "name")]
		public String Name { get; set; }
	}
}
