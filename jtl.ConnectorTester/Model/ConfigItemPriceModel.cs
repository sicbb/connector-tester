using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ConfigItemPriceModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "configItemId")]
		public Identity ConfigItemId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerGroupId")]
		public Identity CustomerGroupId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "price")]
		public Double Price { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "type")]
		public Int32 Type { get; set; }
	}
}
