using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CustomerGroupPackagingQuantitiesModel : MainModel
    {
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customergroupID")]
		public Int32 CustomergroupID { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "minimumOrderQuantity")]
		public Double MinimumOrderQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "packagingQuantity")]
		public Double PackagingQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Int32 ProductId { get; set; }
	}
}
