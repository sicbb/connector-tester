using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CustomerGroupAttrModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerGroupId")]
		public Identity CustomerGroupId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "key")]
		public String Key { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "value")]
		public String Value { get; set; }
	}
}
