using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ConfigGroupModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "comment")]
		public String Comment { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "imagePath")]
		public String ImagePath { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "maximumSelection")]
		public Int32 MaximumSelection { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "minimumSelection")]
		public Int32 MinimumSelection { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sort")]
		public Int32 Sort { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "type")]
		public Int32 Type { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<ConfigGroupI18nModel> I18ns { get; set; }
	}
}
