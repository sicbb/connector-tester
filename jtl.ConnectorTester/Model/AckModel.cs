﻿using System.Collections.Generic;
using jtl.ConnectorTester.Core.Model;
using jtl.ConnectorTester.Model.Interface;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class AckModel
    {
        [JsonProperty(Required = Required.Always, PropertyName = "identities")]
        public IDictionary<ModelType, List<Identity>> Identities { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "checksums")]
        public List<IChecksum> Checksums { get; set; }

        public AckModel()
        {
            Identities = new Dictionary<ModelType, List<Identity>>();
        }
    }
}
