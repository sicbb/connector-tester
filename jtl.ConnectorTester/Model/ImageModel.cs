using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ImageModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "foreignKey")]
		public Identity ForeignKey { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "filename")]
		public String Filename { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "name")]
        public String Name { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "relationType")]
		public ImageRelationTypeModel RelationType { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "remoteUrl")]
		public String RemoteUrl { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sort")]
		public Int32 Sort { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
        public List<ImageI18nModel> I18ns { get; set; }
	}
}
