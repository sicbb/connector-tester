﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace jtl.ConnectorTester.Model
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ImageRelationTypeModel
    {
        [EnumMember(Value = "product")]
        Product,
        [EnumMember(Value = "category")]
        Category,
        [EnumMember(Value = "manufacturer")]
        Manufacturer,
        [EnumMember(Value = "specific")]
        Specific,
        [EnumMember(Value = "specificValue")]
        SpecificValue,
        [EnumMember(Value = "configGroup")]
        ConfigGroup,
        [EnumMember(Value = "productVariationValue")]
        ProductVariationValue
    }
}
