using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductSpecialPriceModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "activeFromDate")]
		public DateTime? ActiveFromDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "activeUntilDate")]
		public DateTime? ActiveUntilDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "considerDateLimit")]
		public Boolean ConsiderDateLimit { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "considerStockLimit")]
		public Boolean ConsiderStockLimit { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isActive")]
		public Boolean IsActive { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "stockLimit")]
		public Int32 StockLimit { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "items")]
		public List<ProductSpecialPriceItemModel> Items { get; set; }
	}
}
