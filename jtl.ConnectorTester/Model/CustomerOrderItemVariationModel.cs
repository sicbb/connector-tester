using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CustomerOrderItemVariationModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerOrderItemId")]
		public Identity CustomerOrderItemId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productVariationId")]
		public Identity ProductVariationId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productVariationValueId")]
		public Identity ProductVariationValueId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "freeField")]
		public String FreeField { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "productVariationName")]
		public String ProductVariationName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "surcharge")]
		public Decimal Surcharge { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "valueName")]
		public String ValueName { get; set; }
	}
}
