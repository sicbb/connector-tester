using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class EmailTemplateModel : MainModel
    {
        [JsonProperty(Required = Required.Always, PropertyName = "id")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity Id { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "emailType")]
        public string EmailType { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "moduleId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity ModuleId { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "filename")]
        public string Filename { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "isActive")]
        public bool IsActive { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "isOii")]
        public bool IsOii { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "isAgb")]
        public bool IsAgb { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "isWrb")]
        public bool IsWrb { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "error")]
        public int Error { get; set; }

    }
}