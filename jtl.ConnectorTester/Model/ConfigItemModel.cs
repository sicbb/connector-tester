using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ConfigItemModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "configGroupId")]
		public Identity ConfigGroupId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "ignoreMultiplier")]
		public Int32 IgnoreMultiplier { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "inheritProductName")]
		public Boolean InheritProductName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "inheritProductPrice")]
		public Boolean InheritProductPrice { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "initialQuantity")]
		public Double InitialQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isPreSelected")]
		public Boolean IsPreSelected { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isRecommended")]
		public Boolean IsRecommended { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "maxQuantity")]
		public Double MaxQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "minQuantity")]
		public Double MinQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "showDiscount")]
		public Boolean ShowDiscount { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "showSurcharge")]
		public Boolean ShowSurcharge { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sort")]
		public Int32 Sort { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "type")]
		public Int32 Type { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<ConfigItemI18nModel> I18ns { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "prices")]
		public List<ConfigItemPriceModel> Prices { get; set; }
	}
}
