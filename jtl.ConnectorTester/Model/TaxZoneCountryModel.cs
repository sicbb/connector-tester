using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class TaxZoneCountryModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "taxZoneId")]
		public Identity TaxZoneId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "countryIso")]
		public String CountryIso { get; set; }
	}
}
