using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class EmailTemplateI18nModel : MainModel
    {
        [JsonProperty(Required = Required.Always, PropertyName = "emailTemplateId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity EmailTemplateId { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "localeName")]
        public string LocaleName { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "subject")]
        public string Subject { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "contentHtml")]
        public string ContentHtml { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "contentText")]
        public string ContentText { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "pdf")]
        public string Pdf { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "filename")]
        public string Filename { get; set; }

    }
}