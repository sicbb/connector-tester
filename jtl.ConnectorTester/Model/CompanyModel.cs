using System;
using System.Collections.Generic;
using System.Windows.Forms;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CompanyModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "accountHolder")]
		public String AccountHolder { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "accountNumber")]
		public String AccountNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "bankCode")]
		public String BankCode { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "bankName")]
		public String BankName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "bic")]
		public String Bic { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "businessman")]
		public String Businessman { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "city")]
		public String City { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "countryIso")]
		public String CountryIso { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "eMail")]
		public String EMail { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "fax")]
		public String Fax { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "iban")]
		public String Iban { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "name")]
		public String Name { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "phone")]
		public String Phone { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "street")]
		public String Street { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "taxIdNumber")]
		public String TaxIdNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "vatNumber")]
		public String VatNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "websiteUrl")]
		public String WebsiteUrl { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "zipCode")]
		public String ZipCode { get; set; }
	}
}
