﻿using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class StatusChangeModel : MainModel
    {
        [JsonConverter(typeof(IdentityConverter))]
        [JsonProperty(Required = Required.Always, PropertyName = "customerOrderId")]
        public Identity CustomerOrderId { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "paymentStatus")]
        public string PaymentStatus { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "orderStatus")]
        public string OrderStatus { get; set; }

        public StatusChangeModel()
        {
            CustomerOrderId = new Identity(1);
            PaymentStatus = string.Empty;
            OrderStatus = string.Empty;
        }
    }
}
