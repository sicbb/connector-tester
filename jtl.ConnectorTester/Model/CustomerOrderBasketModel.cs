using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CustomerOrderBasketModel : MainModel
    {
        [JsonProperty(Required = Required.Always, PropertyName = "id")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity Id { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "customerId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity CustomerId { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "shippingAddressId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity ShippingAddressId { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "customerOrderPaymentInfoId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity CustomerOrderPaymentInfoId { get; set; }

    }
}