using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class PaymentModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerOrderId")]
		public Identity CustomerOrderId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "billingInfo")]
		public String BillingInfo { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creationDate")]
		public DateTime? CreationDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "paymentModuleCode")]
		public String PaymentModuleCode { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "totalSum")]
		public Decimal TotalSum { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "transactionId")]
		public String TransactionId { get; set; }
	}
}
