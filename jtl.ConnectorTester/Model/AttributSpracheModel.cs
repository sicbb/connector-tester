using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class AttributSpracheModel : MainModel
    {
		[JsonProperty(Required = Required.AllowNull, PropertyName = "cWertVarchar")]
		public String cWertVarchar { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "dWertDateTime")]
		public DateTime? dWertDateTime { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "fWertDecimal")]
		public Double fWertDecimal { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "key")]
		public Int32 Key { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "kSprache")]
		public Int32 kSprache { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "nWertInt")]
		public Int32 nWertInt { get; set; }
	}
}
