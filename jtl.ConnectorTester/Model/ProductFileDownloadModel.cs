using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductFileDownloadModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creationDate")]
		public DateTime? CreationDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "maxDays")]
		public Int32 MaxDays { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "maxDownloads")]
		public Int32 MaxDownloads { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "path")]
		public String Path { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "previewPath")]
		public String PreviewPath { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sort")]
		public Int32 Sort { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<ProductFileDownloadI18nModel> I18ns { get; set; }
	}
}
