using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class DeliveryNoteItemInfoModel : MainModel
    {
		[JsonProperty(Required = Required.AllowNull, PropertyName = "batch")]
		public String Batch { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "bestBefore")]
		public DateTime? BestBefore { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "quantity")]
		public Decimal Quantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "warehouseId")]
		public Int32 WarehouseId { get; set; }
	}
}
