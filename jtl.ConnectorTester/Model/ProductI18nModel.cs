using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductI18nModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "deliveryStatus")]
		public String DeliveryStatus { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "description")]
		public String Description { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "languageISO")]
		public String LanguageISO { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "measurementUnitName")]
		public String MeasurementUnitName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "metaDescription")]
		public String MetaDescription { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "metaKeywords")]
		public String MetaKeywords { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "name")]
		public String Name { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "shortDescription")]
		public String ShortDescription { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "titleTag")]
		public String TitleTag { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "unitName")]
		public String UnitName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "urlPath")]
		public String UrlPath { get; set; }
	}
}
