using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CrossSellingItemModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "crossSellingGroupId")]
		public Identity CrossSellingGroupId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "productIds")]
		public List<Identity> ProductIds { get; set; }
	}
}
