using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CategoryAttrI18nModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "categoryAttrId")]
		public Identity CategoryAttrId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "languageISO")]
		public String LanguageISO { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "name")]
		public String Name { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "value")]
		public Object Value { get; set; }
	}
}
