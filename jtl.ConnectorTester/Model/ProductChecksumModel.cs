﻿using jtl.ConnectorTester.Model.Interface;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductChecksumModel : IChecksum
    {
        [JsonProperty(Required = Required.Always, PropertyName = "foreignKey")]
        public Identity ForeignKey { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "endpoint")]
        public string Endpoint { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "host")]
        public string Host { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "type")]
        public int Type { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "hasChanged")]
        public bool HasChanged { get; set; }
    }
}
