using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ShipmentModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "deliveryNoteId")]
		public Identity DeliveryNoteId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "carrierName")]
		public String CarrierName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creationDate")]
		public DateTime? CreationDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "identCode")]
		public String IdentCode { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "note")]
		public String Note { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "trackingUrl")]
		public String TrackingUrl { get; set; }
	}
}
