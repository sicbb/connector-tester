using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class GlobalDataModel : MainModel
    {
		[JsonProperty(Required = Required.AllowNull, PropertyName = "configGroups")]
		public List<ConfigGroupModel> ConfigGroups { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "configItems")]
		public List<ConfigItemModel> ConfigItems { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "crossSellingGroups")]
		public List<CrossSellingGroupModel> CrossSellingGroups { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "currencies")]
		public List<CurrencyModel> Currencies { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerGroups")]
		public List<CustomerGroupModel> CustomerGroups { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "languages")]
		public List<LanguageModel> Languages { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "shippingClasses")]
		public List<ShippingClassModel> ShippingClasses { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "units")]
		public List<UnitModel> Units { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "warehouses")]
		public List<WarehouseModel> Warehouses { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "measurementUnits")]
		public List<MeasurementUnitModel> MeasurementUnits { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "productTypes")]
		public List<ProductTypeModel> ProductTypes { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "taxRates")]
		public List<TaxRateModel> TaxRates { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "shippingMethods")]
		public List<ShippingMethodModel> ShippingMethods { get; set; }
	}
}
