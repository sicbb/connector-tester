using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class SpecificModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isGlobal")]
		public Boolean IsGlobal { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sort")]
		public Int32 Sort { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "type")]
		public String Type { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<SpecificI18nModel> I18ns { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "values")]
		public List<SpecificValueModel> Values { get; set; }
	}
}
