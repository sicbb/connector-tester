using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ConnectorIdentificationModel : MainModel
    {
		[JsonProperty(Required = Required.AllowNull, PropertyName = "endpointVersion")]
		public String EndpointVersion { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "platformName")]
		public String PlatformName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "platformVersion")]
		public String PlatformVersion { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "protocolVersion")]
		public Int32 ProtocolVersion { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "serverInfo")]
		public ConnectorServerInfoModel ServerInfo { get; set; }
	}
}
