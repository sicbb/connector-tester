using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "basePriceUnitId")]
		public Identity BasePriceUnitId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "manufacturerId")]
		public Identity ManufacturerId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "masterProductId")]
		public Identity MasterProductId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "measurementUnitId")]
		public Identity MeasurementUnitId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "partsListId")]
		public Identity PartsListId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productTypeId")]
		public Identity ProductTypeId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "shippingClassId")]
		public Identity ShippingClassId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "unitId")]
		public Identity UnitId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "asin")]
		public String Asin { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "availableFrom")]
		public DateTime? AvailableFrom { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "basePriceDivisor")]
		public Double BasePriceDivisor { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "basePriceFactor")]
		public Decimal BasePriceFactor { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "basePriceQuantity")]
		public Double BasePriceQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "basePriceUnitCode")]
		public String BasePriceUnitCode { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "basePriceUnitName")]
		public String BasePriceUnitName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "considerBasePrice")]
		public Boolean ConsiderBasePrice { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "considerStock")]
		public Boolean ConsiderStock { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "considerVariationStock")]
		public Boolean ConsiderVariationStock { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creationDate")]
		public DateTime? CreationDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "ean")]
		public String Ean { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "epid")]
		public String Epid { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "hazardIdNumber")]
		public String HazardIdNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "height")]
		public Double Height { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isActive")]
		public Boolean IsActive { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isBatch")]
		public Boolean IsBatch { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isBestBefore")]
		public Boolean IsBestBefore { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isbn")]
		public String Isbn { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isDivisible")]
		public Boolean IsDivisible { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isMasterProduct")]
		public Boolean IsMasterProduct { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isNewProduct")]
		public Boolean isNewProduct { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isSerialNumber")]
		public Boolean IsSerialNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isTopProduct")]
		public Boolean IsTopProduct { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "keywords")]
		public String Keywords { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "length")]
		public Double Length { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "manufacturerNumber")]
		public String ManufacturerNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "measurementQuantity")]
		public Double MeasurementQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "measurementUnitCode")]
		public String MeasurementUnitCode { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "minBestBeforeDate")]
		public DateTime? MinBestBeforeDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "minimumOrderQuantity")]
		public Double MinimumOrderQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "minimumQuantity")]
		public Double MinimumQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "modified")]
		public DateTime? Modified { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "nextAvailableInflowDate")]
		public DateTime? NextAvailableInflowDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "nextAvailableInflowQuantity")]
		public Double NextAvailableInflowQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "note")]
		public String Note { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "originCountry")]
		public String OriginCountry { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "packagingQuantity")]
		public Double PackagingQuantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "permitNegativeStock")]
		public Boolean PermitNegativeStock { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "productWeight")]
		public Double ProductWeight { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "purchasePrice")]
		public Double PurchasePrice { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "recommendedRetailPrice")]
		public Double RecommendedRetailPrice { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "serialNumber")]
		public String SerialNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "shippingWeight")]
		public Double ShippingWeight { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sku")]
		public String Sku { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sort")]
		public Int32 Sort { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "stockLevel")]
		public ProductStockLevelModel StockLevel { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "supplierDeliveryTime")]
		public Int32 SupplierDeliveryTime { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "supplierStockLevel")]
		public Decimal SupplierStockLevel { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "taric")]
		public String Taric { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "unNumber")]
		public String UnNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "upc")]
		public String Upc { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "vat")]
		public Decimal Vat { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "width")]
		public Double Width { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "categories")]
		public List<Product2CategoryModel> Categories { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "configGroups")]
		public List<ProductConfigGroupModel> ConfigGroups { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "fileDownloads")]
		public List<ProductFileDownloadModel> FileDownloads { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<ProductI18nModel> I18ns { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "invisibilities")]
		public List<ProductInvisibilityModel> Invisibilities { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "mediaFiles")]
		public List<ProductMediaFileModel> MediaFiles { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "prices")]
		public List<ProductPriceModel> Prices { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "partsLists")]
		public List<ProductPartsListModel> PartsLists { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "attributes")]
		public List<ProductAttrModel> Attributes { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "specialPrices")]
		public List<ProductSpecialPriceModel> SpecialPrices { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "specifics")]
		public List<ProductSpecificModel> Specifics { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "warehouseInfo")]
		public List<ProductWarehouseInfoModel> WarehouseInfo { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "variations")]
		public List<ProductVariationModel> Variations { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "checksums")]
		public List<ProductChecksumModel> Checksums { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "varCombinations")]
		public List<ProductVarCombinationModel> VarCombinations { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerGroupPackagingQuantities")]
		public List<CustomerGroupPackagingQuantityModel> CustomerGroupPackagingQuantities { get; set; }
	}
}
