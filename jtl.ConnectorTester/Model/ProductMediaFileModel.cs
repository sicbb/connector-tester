using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductMediaFileModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "mediaFileCategory")]
		public String MediaFileCategory { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "path")]
		public String Path { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sort")]
		public Int16 Sort { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "type")]
		public String Type { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "url")]
		public String Url { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<ProductMediaFileI18nModel> I18ns { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "attributes")]
		public List<ProductMediaFileAttrModel> Attributes { get; set; }
	}
}
