using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CustomerOrderShippingAddressModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerId")]
		public Identity CustomerId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "city")]
		public String City { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "company")]
		public String Company { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "countryIso")]
		public String CountryIso { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "deliveryInstruction")]
		public String DeliveryInstruction { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "eMail")]
		public String EMail { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "extraAddressLine")]
		public String ExtraAddressLine { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "fax")]
		public String Fax { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "firstName")]
		public String FirstName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "lastName")]
		public String LastName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "mobile")]
		public String Mobile { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "phone")]
		public String Phone { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "salutation")]
		public String Salutation { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "state")]
		public String State { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "street")]
		public String Street { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "title")]
		public String Title { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "zipCode")]
		public String ZipCode { get; set; }
	}
}
