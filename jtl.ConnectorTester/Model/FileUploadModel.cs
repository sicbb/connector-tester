using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class FileUploadModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "fileType")]
		public String FileType { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isRequired")]
		public Boolean IsRequired { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<FileUploadI18nModel> I18ns { get; set; }
	}
}
