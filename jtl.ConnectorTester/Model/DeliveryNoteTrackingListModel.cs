using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class DeliveryNoteTrackingListModel : MainModel
    {
		[JsonProperty(Required = Required.AllowNull, PropertyName = "name")]
		public String Name { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "codes")]
		public List<string> Codes { get; set; }
	}
}
