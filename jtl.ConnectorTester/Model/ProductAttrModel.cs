using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductAttrModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isCustomProperty")]
		public Boolean IsCustomProperty { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isTranslated")]
		public Boolean IsTranslated { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<ProductAttrI18nModel> I18ns { get; set; }
	}
}
