using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductPriceItemModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productPriceId")]
		public Identity ProductPriceId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "netPrice")]
		public Double NetPrice { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "quantity")]
		public Int32 Quantity { get; set; }
	}
}
