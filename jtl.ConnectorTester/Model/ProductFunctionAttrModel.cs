using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ProductFunctionAttrModel : MainModel
    {
        [JsonProperty(Required = Required.Always, PropertyName = "id")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity Id { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "productId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity ProductId { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "key")]
        public string Key { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "value")]
        public string Value { get; set; }

    }
}