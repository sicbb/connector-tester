using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CustomerOrderItemModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "configItemId")]
		public Identity ConfigItemId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerOrderId")]
		public Identity CustomerOrderId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
		public Identity ProductId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "name")]
		public String Name { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "price")]
		public Double Price { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "quantity")]
		public Double Quantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sku")]
		public String Sku { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "type")]
		public String Type { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "unique")]
		public String Unique { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "vat")]
		public Double Vat { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "variations")]
		public List<CustomerOrderItemVariationModel> Variations { get; set; }
	}
}
