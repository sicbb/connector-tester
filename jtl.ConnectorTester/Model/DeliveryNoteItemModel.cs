using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class DeliveryNoteItemModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerOrderItemId")]
		public Identity CustomerOrderItemId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "deliveryNoteId")]
		public Identity DeliveryNoteId { get; set; }

        [JsonConverter(typeof(IdentityConverter))]
        [JsonProperty(Required = Required.AllowNull, PropertyName = "productId")]
        public Identity ProductId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "quantity")]
		public Double Quantity { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "info")]
		public List<DeliveryNoteItemInfoModel> Info { get; set; }
	}
}
