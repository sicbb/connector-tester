using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class WarehouseI18nModel : MainModel
    {
        [JsonProperty(Required = Required.Always, PropertyName = "warehouseId")]
        [JsonConverter(typeof(IdentityConverter))]
        public Identity WarehouseId { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "name")]
        public string Name { get; set; }

    }
}