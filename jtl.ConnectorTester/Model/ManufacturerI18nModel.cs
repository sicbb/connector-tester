using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class ManufacturerI18nModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "manufacturerId")]
		public Identity ManufacturerId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "description")]
		public String Description { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "languageISO")]
		public String LanguageISO { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "metaDescription")]
		public String MetaDescription { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "metaKeywords")]
		public String MetaKeywords { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "titleTag")]
		public String TitleTag { get; set; }
	}
}
