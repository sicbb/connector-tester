using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CustomerOrderModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerId")]
		public Identity CustomerId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "billingAddress")]
		public CustomerOrderBillingAddressModel BillingAddress { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "carrierName")]
		public String CarrierName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creationDate")]
		public DateTime? CreationDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "currencyIso")]
		public String CurrencyIso { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "estimatedDeliveryDate")]
		public DateTime? EstimatedDeliveryDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "languageISO")]
		public String LanguageISO { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "note")]
		public String Note { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "orderNumber")]
		public String OrderNumber { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "paymentDate")]
		public DateTime? PaymentDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "paymentInfo")]
		public CustomerOrderPaymentInfoModel PaymentInfo { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "paymentModuleCode")]
		public String PaymentModuleCode { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "paymentStatus")]
		public String PaymentStatus { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "pui")]
		public String Pui { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "shippingAddress")]
		public CustomerOrderShippingAddressModel ShippingAddress { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "shippingDate")]
		public DateTime? ShippingDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "shippingInfo")]
		public String ShippingInfo { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "shippingMethodName")]
		public String ShippingMethodName { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "status")]
		public String Status { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "totalSum")]
		public Decimal TotalSum { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "attributes")]
		public List<CustomerOrderAttrModel> Attributes { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "items")]
		public List<CustomerOrderItemModel> Items { get; set; }
	}
}
