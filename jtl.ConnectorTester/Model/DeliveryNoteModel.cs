using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class DeliveryNoteModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerOrderId")]
		public Identity CustomerOrderId { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "creationDate")]
		public DateTime? CreationDate { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isFulfillment")]
		public Boolean IsFulfillment { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "note")]
		public String Note { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "items")]
		public List<DeliveryNoteItemModel> Items { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "trackingLists")]
		public List<DeliveryNoteTrackingListModel> TrackingLists { get; set; }
	}
}
