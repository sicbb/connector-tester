using System;
using System.Collections.Generic;
using jtl.ConnectorTester.Core;
using jtl.ConnectorTester.Core.Model;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    public class CategoryModel : MainModel
    {
		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "id")]
		public Identity Id { get; set; }

		[JsonConverter(typeof(IdentityConverter))]
		[JsonProperty(Required = Required.AllowNull, PropertyName = "parentCategoryId")]
		public Identity ParentCategoryId { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "isActive")]
		public Boolean IsActive { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "level")]
		public Int32 Level { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "sort")]
		public Int32 Sort { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "attributes")]
		public List<CategoryAttrModel> Attributes { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "customerGroups")]
		public List<CategoryCustomerGroupModel> CustomerGroups { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "i18ns")]
		public List<CategoryI18nModel> I18ns { get; set; }

		[JsonProperty(Required = Required.AllowNull, PropertyName = "invisibilities")]
		public List<CategoryInvisibilityModel> Invisibilities { get; set; }
	}
}
