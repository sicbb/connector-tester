﻿using System;
using jtl.ConnectorTester.Core;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Model
{
    [JsonConverter(typeof(IdentityConverter))]
    public class Identity
    {
        [JsonProperty(Required = Required.Always, PropertyName = "endpoint")]
        public string Endpoint { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "host")]
        public int Host { get; set; }

        public Identity()
        {
            Endpoint = String.Empty;
            Host = 0;
        }

        public Identity(object endpoint)
        {
            Endpoint = endpoint.ToString();
            Host = 0;
        }

        public Identity(object endpoint, object host)
        {
            Endpoint = endpoint.ToString();
            Host = Convert.ToInt32(host);
        }

        public void Set(object endpoint, object host)
        {
            Endpoint = endpoint.ToString();
            Host = Convert.ToInt32(host);
        }
    }
}
