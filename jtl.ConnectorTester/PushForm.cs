﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using jtl.ConnectorTester.Core.Model;
using jtl.ConnectorTester.Core.Rpc;

namespace jtl.ConnectorTester
{
    public partial class PushForm : Form
    {
        public delegate void InvalidSessionHandler();
        public event InvalidSessionHandler InvalidSessionEvent;

        public MainModel Model { get; protected set; }
        private readonly Form1 _mainForm;
        public string ModelType { get; protected set; }

        public PushForm(MainModel model, Form1 form, string modelType)
        {
            InitializeComponent();
            Model = model;
            _mainForm = form;
            ModelType = modelType;
            dialogHeader1.Text = string.Format("{0} Model", modelType);
        }

        private void ContainerForm_Shown(object sender, EventArgs e)
        {
            rtbResponse.Text = Model.ToString();
            cmbAction.SelectedIndex = 2;
            _mainForm.Connector.Picture = null;
            _mainForm.Connector.Picturename = null;
            _mainForm.Connector.Images = new List<Image>();
            dgvImages.DataSource = new BindingSource { DataSource = _mainForm.Connector.Images };

            dgvImages.CellEndEdit += (s, a) =>
            {
                _mainForm.Connector.Images = (List<Image>)((BindingSource)dgvImages.DataSource).DataSource;
            };

            dgvImages.UserDeletedRow += (s, a) =>
            {
                _mainForm.Connector.Images = (List<Image>)((BindingSource)dgvImages.DataSource).DataSource;
            };

            btnFile.Enabled = (Model.GetType().FullName == "jtl.ConnectorTester.Model.ImageModel");
        }

        private void tcResponse_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tcResponse.SelectedTab.Text)
            {
                case "Tree":
                    _mainForm.BuildTree(rtbResponse.Text, tvResponse);
                    break;
            }
        }

        private async void DoAction(Func<Task<RpcResponse>> connectorCall, Action<RpcResponse> guiAction)
        {
            try
            {
                var response = await connectorCall();
                guiAction(response);
            }
            catch (Exception ex)
            {
                if (!Form1.IsSessionValid(ex.Message) && InvalidSessionEvent != null)
                {
                    InvalidSessionEvent();
                    Close();
                }

                rtbResponse2.Text += ex.Message + Environment.NewLine;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            rtbResponse2.Text = string.Empty;

            try
            {
                if (rtbResponse.Text.Trim()[0] == '[')
                {
                    var list = MainModel.DeserializeList(ModelType, rtbResponse.Text);

                    DoAction(() => _mainForm.Connector.PushListAsync(_mainForm.Url, ModelType, list), res =>
                    {
                        rtbResponse2.Text += res.ToString();
                    });
                }
                else
                {
                    Model = MainModel.Deserialize(ModelType, rtbResponse.Text) as MainModel;

                    DoAction(() => _mainForm.Connector.PushAsync(_mainForm.Url, ModelType, Model, (ModelType != "Image")), res =>
                    {
                        rtbResponse2.Text += res.ToString();
                    });
                }
            }
            catch (Exception exc)
            {
                if (!Form1.IsSessionValid(exc.Message) && InvalidSessionEvent != null)
                {
                    InvalidSessionEvent();
                    Close();
                }

                rtbResponse2.Text += exc.Message + Environment.NewLine;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tcResponse2_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tcResponse2.SelectedTab.Text)
            {
                case "Tree":
                    _mainForm.BuildTree(rtbResponse2.Text, tvResponse2);
                    break;
                case "HTML":
                    var i = rtbResponse2.Text.IndexOf("Http Response:", StringComparison.Ordinal);

                    webBrowser1.DocumentText = i != -1 ? rtbResponse2.Text.Substring(i).Replace("Http Response:", "") : rtbResponse2.Text;
                    break;
            }
        }

        private void cmbAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Model = MainModel.Deserialize(ModelType, rtbResponse.Text) as MainModel;

                if (Model != null)
                {
                    //Model.ActionChange(cmbAction.Text);

                    rtbResponse.Text = Model.ToString();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;

            _mainForm.Connector.Picture = File.ReadAllBytes(openFileDialog1.FileName);
            _mainForm.Connector.Picturename = openFileDialog1.SafeFileName;
            var fileInfo = new FileInfo(openFileDialog1.FileName);
            var count = _mainForm.Connector.Images.Count;
            _mainForm.Connector.Images.Add(new Image { HostId = count + 1, FilePath = openFileDialog1.FileName, FileName = openFileDialog1.SafeFileName, Extension = fileInfo.Extension });
            dgvImages.DataSource = new BindingSource { DataSource = _mainForm.Connector.Images };
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            rtbResponse2.Text = string.Empty;

            try
            {
                Model = MainModel.Deserialize(ModelType, rtbResponse.Text) as MainModel;

                DoAction(() => _mainForm.Connector.DeleteAsync(_mainForm.Url, ModelType, Model, (ModelType != "Image")), res =>
                {
                    rtbResponse2.Text += res.ToString();
                });
            }
            catch (Exception exc)
            {
                if (!Form1.IsSessionValid(exc.Message) && InvalidSessionEvent != null)
                {
                    InvalidSessionEvent();
                    Close();
                }

                rtbResponse2.Text += exc.Message + Environment.NewLine;
            }
        }
    }
}
