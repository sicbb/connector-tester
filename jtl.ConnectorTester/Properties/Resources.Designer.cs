﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace jtl.ConnectorTester.Properties {
    using System;
    
    
    /// <summary>
    ///   Eine stark typisierte Ressourcenklasse zum Suchen von lokalisierten Zeichenfolgen usw.
    /// </summary>
    // Diese Klasse wurde von der StronglyTypedResourceBuilder automatisch generiert
    // -Klasse über ein Tool wie ResGen oder Visual Studio automatisch generiert.
    // Um einen Member hinzuzufügen oder zu entfernen, bearbeiten Sie die .ResX-Datei und führen dann ResGen
    // mit der /str-Option erneut aus, oder Sie erstellen Ihr VS-Projekt neu.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Gibt die zwischengespeicherte ResourceManager-Instanz zurück, die von dieser Klasse verwendet wird.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("jtl.ConnectorTester.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Überschreibt die CurrentUICulture-Eigenschaft des aktuellen Threads für alle
        ///   Ressourcenzuordnungen, die diese stark typisierte Ressourcenklasse verwenden.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap action {
            get {
                object obj = ResourceManager.GetObject("action", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap apple_logo {
            get {
                object obj = ResourceManager.GetObject("apple_logo", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap auth {
            get {
                object obj = ResourceManager.GetObject("auth", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die [{&quot;id&quot;:[&quot;&quot;,1],&quot;parentCategoryId&quot;:[&quot;&quot;,0],&quot;isActive&quot;:true,&quot;level&quot;:0,&quot;sort&quot;:0,&quot;attributes&quot;:[{&quot;categoryId&quot;:[&quot;&quot;,1],&quot;id&quot;:[&quot;&quot;,1],&quot;isTranslated&quot;:false,&quot;isCustomProperty&quot;:false,&quot;i18ns&quot;:[{&quot;categoryAttrId&quot;:[&quot;&quot;,1],&quot;languageISO&quot;:&quot;ger&quot;,&quot;name&quot;:&quot;attribute1&quot;,&quot;value&quot;:&quot;Hallo Frank&quot;}]},{&quot;categoryId&quot;:[&quot;&quot;,1],&quot;id&quot;:[&quot;&quot;,2],&quot;isTranslated&quot;:false,&quot;isCustomProperty&quot;:false,&quot;i18ns&quot;:[{&quot;categoryAttrId&quot;:[&quot;&quot;,2],&quot;languageISO&quot;:&quot;ger&quot;,&quot;name&quot;:&quot;attribute2&quot;,&quot;value&quot;:&quot;Dies ist ein Test&quot;}]}],&quot;customerGroups&quot;:[],&quot;i18ns&quot;:[{&quot;categoryId&quot;:[&quot;&quot;,1],&quot;descripti [Rest der Zeichenfolge wurde abgeschnitten]&quot;; ähnelt.
        /// </summary>
        internal static string CategoryMock {
            get {
                return ResourceManager.GetString("CategoryMock", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap disconnect {
            get {
                object obj = ResourceManager.GetObject("disconnect", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die {
        ///    &quot;foreignKey&quot;: [
        ///        &quot;&quot;,
        ///        2
        ///    ],
        ///    &quot;id&quot;: [
        ///        &quot;&quot;,
        ///        1
        ///    ],
        ///    &quot;filename&quot;: &quot;Apple_logo.jpg&quot;,
        ///    &quot;relationType&quot;: &quot;category&quot;,
        ///    &quot;remoteUrl&quot;: &quot;Apple_logo.jpg&quot;,
        ///    &quot;sort&quot;: 1
        ///} ähnelt.
        /// </summary>
        internal static string ImageMockApple {
            get {
                return ResourceManager.GetString("ImageMockApple", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die {
        ///    &quot;foreignKey&quot;: [
        ///        &quot;&quot;,
        ///        4
        ///    ],
        ///    &quot;id&quot;: [
        ///        &quot;&quot;,
        ///        2
        ///    ],
        ///    &quot;filename&quot;: &quot;ladekabel.png&quot;,
        ///    &quot;relationType&quot;: &quot;product&quot;,
        ///    &quot;remoteUrl&quot;: &quot;ladekabel.png&quot;,
        ///    &quot;sort&quot;: 1
        ///} ähnelt.
        /// </summary>
        internal static string ImageMockCable {
            get {
                return ResourceManager.GetString("ImageMockCable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die {
        ///    &quot;foreignKey&quot;: [
        ///        &quot;&quot;,
        ///        1
        ///    ],
        ///    &quot;id&quot;: [
        ///        &quot;&quot;,
        ///        3
        ///    ],
        ///    &quot;filename&quot;: &quot;iphone6.png&quot;,
        ///    &quot;relationType&quot;: &quot;product&quot;,
        ///    &quot;remoteUrl&quot;: &quot;iphone6.png&quot;,
        ///    &quot;sort&quot;: 1
        ///} ähnelt.
        /// </summary>
        internal static string ImageMockPhone {
            get {
                return ResourceManager.GetString("ImageMockPhone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die {
        ///    &quot;foreignKey&quot;: [
        ///        &quot;&quot;,
        ///        2
        ///    ],
        ///    &quot;id&quot;: [
        ///        &quot;&quot;,
        ///        4
        ///    ],
        ///    &quot;filename&quot;: &quot;iPhone6_gold.png&quot;,
        ///    &quot;relationType&quot;: &quot;product&quot;,
        ///    &quot;remoteUrl&quot;: &quot;iPhone6_gold.png&quot;,
        ///    &quot;sort&quot;: 1
        ///} ähnelt.
        /// </summary>
        internal static string ImageMockPhoneGold {
            get {
                return ResourceManager.GetString("ImageMockPhoneGold", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die {
        ///    &quot;foreignKey&quot;: [
        ///        &quot;&quot;,
        ///        3
        ///    ],
        ///    &quot;id&quot;: [
        ///        &quot;&quot;,
        ///        5
        ///    ],
        ///    &quot;filename&quot;: &quot;iPhone6_grey.png&quot;,
        ///    &quot;relationType&quot;: &quot;product&quot;,
        ///    &quot;remoteUrl&quot;: &quot;iPhone6_grey.png&quot;,
        ///    &quot;sort&quot;: 2
        ///} ähnelt.
        /// </summary>
        internal static string ImageMockPhoneGrey {
            get {
                return ResourceManager.GetString("ImageMockPhoneGrey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap iphone6_gold {
            get {
                object obj = ResourceManager.GetObject("iphone6_gold", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap iphone6_grey {
            get {
                object obj = ResourceManager.GetObject("iphone6_grey", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap iphone6_silver {
            get {
                object obj = ResourceManager.GetObject("iphone6_silver", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ladekabel {
            get {
                object obj = ResourceManager.GetObject("ladekabel", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die [
        ///    {
        ///        &quot;id&quot;: [
        ///            &quot;&quot;,
        ///            1
        ///        ],
        ///        &quot;name&quot;: &quot;Apple&quot;,
        ///        &quot;sort&quot;: 0,
        ///        &quot;urlPath&quot;: &quot;&quot;,
        ///        &quot;websiteUrl&quot;: &quot;&quot;,
        ///        &quot;i18ns&quot;: [
        ///            {
        ///                &quot;manufacturerId&quot;: [
        ///                    &quot;&quot;,
        ///                    1
        ///                ],
        ///                &quot;description&quot;: &quot;Apple Inc&quot;,
        ///                &quot;languageISO&quot;: &quot;ger&quot;,
        ///                &quot;metaDescription&quot;: &quot;&quot;,
        ///                &quot;metaKeywords&quot;: &quot;&quot;,
        ///                &quot;titleTag&quot;: &quot;&quot;
        ///            }
        ///        ]
        ///    }
        ///] ähnelt.
        /// </summary>
        internal static string ManufacturerMock {
            get {
                return ResourceManager.GetString("ManufacturerMock", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap openfolder {
            get {
                object obj = ResourceManager.GetObject("openfolder", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die [
        ///    {
        ///        &quot;basePriceUnitId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;deliveryStatusId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;id&quot;: [
        ///            &quot;&quot;,
        ///            4
        ///        ],
        ///        &quot;manufacturerId&quot;: [
        ///            &quot;&quot;,
        ///            1
        ///        ],
        ///        &quot;masterProductId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;measurementUnitId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;partsListId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;productTypeId&quot;: [
        ///   [Rest der Zeichenfolge wurde abgeschnitten]&quot;; ähnelt.
        /// </summary>
        internal static string ProductMock {
            get {
                return ResourceManager.GetString("ProductMock", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die [
        ///    {
        ///        &quot;basePriceUnitId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;deliveryStatusId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;id&quot;: [
        ///            &quot;&quot;,
        ///            2
        ///        ],
        ///        &quot;manufacturerId&quot;: [
        ///            &quot;&quot;,
        ///            1
        ///        ],
        ///        &quot;masterProductId&quot;: [
        ///            &quot;&quot;,
        ///            1
        ///        ],
        ///        &quot;measurementUnitId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;partsListId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;productTypeId&quot;: [
        ///   [Rest der Zeichenfolge wurde abgeschnitten]&quot;; ähnelt.
        /// </summary>
        internal static string ProductMockChild {
            get {
                return ResourceManager.GetString("ProductMockChild", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die [
        ///    {
        ///        &quot;basePriceUnitId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;deliveryStatusId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;id&quot;: [
        ///            &quot;&quot;,
        ///            4
        ///        ],
        ///        &quot;manufacturerId&quot;: [
        ///            &quot;&quot;,
        ///            1
        ///        ],
        ///        &quot;masterProductId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;measurementUnitId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;partsListId&quot;: [
        ///            &quot;&quot;,
        ///            0
        ///        ],
        ///        &quot;productTypeId&quot;: [
        ///   [Rest der Zeichenfolge wurde abgeschnitten]&quot;; ähnelt.
        /// </summary>
        internal static string ProductMockVar {
            get {
                return ResourceManager.GetString("ProductMockVar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap share {
            get {
                object obj = ResourceManager.GetObject("share", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap start {
            get {
                object obj = ResourceManager.GetObject("start", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap stop {
            get {
                object obj = ResourceManager.GetObject("stop", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
