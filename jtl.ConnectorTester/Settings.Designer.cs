﻿namespace jtl.ConnectorTester
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.dialogHeader1 = new prounion.Controls.DialogHeader();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.gbResolution = new System.Windows.Forms.GroupBox();
            this.nudHeight = new System.Windows.Forms.NumericUpDown();
            this.nudWidth = new System.Windows.Forms.NumericUpDown();
            this.lvlHeight = new System.Windows.Forms.Label();
            this.lvlWidth = new System.Windows.Forms.Label();
            this.gbEndpoint = new System.Windows.Forms.GroupBox();
            this.cbEndpoint = new System.Windows.Forms.ComboBox();
            this.dgvEndpointList = new System.Windows.Forms.DataGridView();
            this.gbEndpointList = new System.Windows.Forms.GroupBox();
            this.gbResolution.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidth)).BeginInit();
            this.gbEndpoint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndpointList)).BeginInit();
            this.gbEndpointList.SuspendLayout();
            this.SuspendLayout();
            // 
            // dialogHeader1
            // 
            this.dialogHeader1.BackColor = System.Drawing.Color.White;
            this.dialogHeader1.Description = "It will make you bulletproof";
            this.dialogHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dialogHeader1.Location = new System.Drawing.Point(0, 0);
            this.dialogHeader1.Margin = new System.Windows.Forms.Padding(0);
            this.dialogHeader1.Name = "dialogHeader1";
            this.dialogHeader1.Size = new System.Drawing.Size(470, 50);
            this.dialogHeader1.TabIndex = 0;
            this.dialogHeader1.Text = "Connector Tester";
            this.dialogHeader1.TextImage = null;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(302, 424);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(383, 424);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // gbResolution
            // 
            this.gbResolution.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbResolution.Controls.Add(this.nudHeight);
            this.gbResolution.Controls.Add(this.nudWidth);
            this.gbResolution.Controls.Add(this.lvlHeight);
            this.gbResolution.Controls.Add(this.lvlWidth);
            this.gbResolution.Location = new System.Drawing.Point(12, 53);
            this.gbResolution.Name = "gbResolution";
            this.gbResolution.Size = new System.Drawing.Size(446, 85);
            this.gbResolution.TabIndex = 3;
            this.gbResolution.TabStop = false;
            this.gbResolution.Text = "Resolution";
            // 
            // nudHeight
            // 
            this.nudHeight.Location = new System.Drawing.Point(50, 49);
            this.nudHeight.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.nudHeight.Name = "nudHeight";
            this.nudHeight.Size = new System.Drawing.Size(85, 20);
            this.nudHeight.TabIndex = 3;
            // 
            // nudWidth
            // 
            this.nudWidth.Location = new System.Drawing.Point(50, 23);
            this.nudWidth.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.nudWidth.Name = "nudWidth";
            this.nudWidth.Size = new System.Drawing.Size(85, 20);
            this.nudWidth.TabIndex = 2;
            // 
            // lvlHeight
            // 
            this.lvlHeight.AutoSize = true;
            this.lvlHeight.Location = new System.Drawing.Point(6, 51);
            this.lvlHeight.Name = "lvlHeight";
            this.lvlHeight.Size = new System.Drawing.Size(41, 13);
            this.lvlHeight.TabIndex = 1;
            this.lvlHeight.Text = "Height:";
            // 
            // lvlWidth
            // 
            this.lvlWidth.AutoSize = true;
            this.lvlWidth.Location = new System.Drawing.Point(6, 25);
            this.lvlWidth.Name = "lvlWidth";
            this.lvlWidth.Size = new System.Drawing.Size(38, 13);
            this.lvlWidth.TabIndex = 0;
            this.lvlWidth.Text = "Width:";
            // 
            // gbEndpoint
            // 
            this.gbEndpoint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbEndpoint.Controls.Add(this.cbEndpoint);
            this.gbEndpoint.Location = new System.Drawing.Point(12, 144);
            this.gbEndpoint.Name = "gbEndpoint";
            this.gbEndpoint.Size = new System.Drawing.Size(446, 85);
            this.gbEndpoint.TabIndex = 4;
            this.gbEndpoint.TabStop = false;
            this.gbEndpoint.Text = "Default Endpoint";
            // 
            // cbEndpoint
            // 
            this.cbEndpoint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbEndpoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEndpoint.FormattingEnabled = true;
            this.cbEndpoint.Location = new System.Drawing.Point(6, 34);
            this.cbEndpoint.Name = "cbEndpoint";
            this.cbEndpoint.Size = new System.Drawing.Size(434, 21);
            this.cbEndpoint.TabIndex = 0;
            // 
            // dgvEndpointList
            // 
            this.dgvEndpointList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEndpointList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEndpointList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEndpointList.Location = new System.Drawing.Point(6, 19);
            this.dgvEndpointList.Name = "dgvEndpointList";
            this.dgvEndpointList.Size = new System.Drawing.Size(434, 158);
            this.dgvEndpointList.TabIndex = 5;
            this.dgvEndpointList.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEndpointList_CellEndEdit);
            this.dgvEndpointList.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvEndpointList_UserDeletedRow);
            // 
            // gbEndpointList
            // 
            this.gbEndpointList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbEndpointList.Controls.Add(this.dgvEndpointList);
            this.gbEndpointList.Location = new System.Drawing.Point(12, 235);
            this.gbEndpointList.Name = "gbEndpointList";
            this.gbEndpointList.Size = new System.Drawing.Size(446, 183);
            this.gbEndpointList.TabIndex = 6;
            this.gbEndpointList.TabStop = false;
            this.gbEndpointList.Text = "List of Endpoints";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 459);
            this.Controls.Add(this.gbEndpointList);
            this.Controls.Add(this.gbEndpoint);
            this.Controls.Add(this.gbResolution);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dialogHeader1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.gbResolution.ResumeLayout(false);
            this.gbResolution.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidth)).EndInit();
            this.gbEndpoint.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEndpointList)).EndInit();
            this.gbEndpointList.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private prounion.Controls.DialogHeader dialogHeader1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox gbResolution;
        private System.Windows.Forms.Label lvlHeight;
        private System.Windows.Forms.Label lvlWidth;
        private System.Windows.Forms.NumericUpDown nudHeight;
        private System.Windows.Forms.NumericUpDown nudWidth;
        private System.Windows.Forms.GroupBox gbEndpoint;
        private System.Windows.Forms.ComboBox cbEndpoint;
        private System.Windows.Forms.DataGridView dgvEndpointList;
        private System.Windows.Forms.GroupBox gbEndpointList;
    }
}