﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace jtl.ConnectorTester
{
    public partial class ValidationError : Form
    {
        public List<string> Errors { get; set; }

        public ValidationError()
        {
            InitializeComponent();
            Errors = new List<string>();
        }

        private void ValidationError_Shown(object sender, EventArgs e)
        {
            tbErrors.Text = String.Join(Environment.NewLine, Errors);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
