﻿namespace jtl.ConnectorTester
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rtbResponse = new System.Windows.Forms.RichTextBox();
            this.gbControl = new System.Windows.Forms.GroupBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnAckClear = new System.Windows.Forms.Button();
            this.btnAck = new System.Windows.Forms.Button();
            this.txtParentId = new System.Windows.Forms.TextBox();
            this.ckbFetchChildren = new System.Windows.Forms.CheckBox();
            this.nudLimit = new System.Windows.Forms.NumericUpDown();
            this.lblLimit = new System.Windows.Forms.Label();
            this.nudOffset = new System.Windows.Forms.NumericUpDown();
            this.lblOffset = new System.Windows.Forms.Label();
            this.btnContainerPush = new System.Windows.Forms.Button();
            this.lblAction = new System.Windows.Forms.Label();
            this.btnAction = new System.Windows.Forms.Button();
            this.cbAction = new System.Windows.Forms.ComboBox();
            this.lblWatch = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nudCount = new System.Windows.Forms.NumericUpDown();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.cboxUrl = new System.Windows.Forms.ComboBox();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSession = new System.Windows.Forms.Button();
            this.tcResponse = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tvResponse = new System.Windows.Forms.TreeView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.dialogHeader1 = new prounion.Controls.DialogHeader();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCount)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tcResponse.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(81, 15);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(228, 21);
            this.cmbType.TabIndex = 1;
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Object Type:";
            // 
            // rtbResponse
            // 
            this.rtbResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbResponse.Location = new System.Drawing.Point(0, -1);
            this.rtbResponse.Name = "rtbResponse";
            this.rtbResponse.Size = new System.Drawing.Size(908, 536);
            this.rtbResponse.TabIndex = 5;
            this.rtbResponse.Text = "";
            // 
            // gbControl
            // 
            this.gbControl.Controls.Add(this.btnTest);
            this.gbControl.Controls.Add(this.btnAckClear);
            this.gbControl.Controls.Add(this.btnAck);
            this.gbControl.Controls.Add(this.txtParentId);
            this.gbControl.Controls.Add(this.ckbFetchChildren);
            this.gbControl.Controls.Add(this.nudLimit);
            this.gbControl.Controls.Add(this.lblLimit);
            this.gbControl.Controls.Add(this.nudOffset);
            this.gbControl.Controls.Add(this.lblOffset);
            this.gbControl.Controls.Add(this.btnContainerPush);
            this.gbControl.Controls.Add(this.lblAction);
            this.gbControl.Controls.Add(this.btnAction);
            this.gbControl.Controls.Add(this.cbAction);
            this.gbControl.Controls.Add(this.lblWatch);
            this.gbControl.Controls.Add(this.label2);
            this.gbControl.Controls.Add(this.nudCount);
            this.gbControl.Controls.Add(this.cmbType);
            this.gbControl.Controls.Add(this.label1);
            this.gbControl.Enabled = false;
            this.gbControl.Location = new System.Drawing.Point(12, 188);
            this.gbControl.Name = "gbControl";
            this.gbControl.Size = new System.Drawing.Size(319, 471);
            this.gbControl.TabIndex = 6;
            this.gbControl.TabStop = false;
            this.gbControl.Text = "Controls";
            // 
            // btnTest
            // 
            this.btnTest.Image = global::jtl.ConnectorTester.Properties.Resources.openfolder;
            this.btnTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTest.Location = new System.Drawing.Point(10, 321);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(299, 43);
            this.btnTest.TabIndex = 31;
            this.btnTest.Text = "Push Test";
            this.btnTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnAckClear
            // 
            this.btnAckClear.Image = global::jtl.ConnectorTester.Properties.Resources.stop;
            this.btnAckClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAckClear.Location = new System.Drawing.Point(102, 247);
            this.btnAckClear.Name = "btnAckClear";
            this.btnAckClear.Size = new System.Drawing.Size(207, 43);
            this.btnAckClear.TabIndex = 30;
            this.btnAckClear.Text = "Clear Linkings";
            this.btnAckClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAckClear.UseVisualStyleBackColor = true;
            this.btnAckClear.Click += new System.EventHandler(this.btnAckClear_Click);
            // 
            // btnAck
            // 
            this.btnAck.Enabled = false;
            this.btnAck.Image = global::jtl.ConnectorTester.Properties.Resources.start;
            this.btnAck.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAck.Location = new System.Drawing.Point(102, 198);
            this.btnAck.Name = "btnAck";
            this.btnAck.Size = new System.Drawing.Size(207, 43);
            this.btnAck.TabIndex = 29;
            this.btnAck.Text = "Trigger ACK";
            this.btnAck.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAck.UseVisualStyleBackColor = true;
            this.btnAck.Click += new System.EventHandler(this.btnAck_Click);
            // 
            // txtParentId
            // 
            this.txtParentId.Location = new System.Drawing.Point(172, 145);
            this.txtParentId.Name = "txtParentId";
            this.txtParentId.Size = new System.Drawing.Size(106, 20);
            this.txtParentId.TabIndex = 6;
            // 
            // ckbFetchChildren
            // 
            this.ckbFetchChildren.AutoSize = true;
            this.ckbFetchChildren.Location = new System.Drawing.Point(81, 147);
            this.ckbFetchChildren.Name = "ckbFetchChildren";
            this.ckbFetchChildren.Size = new System.Drawing.Size(91, 17);
            this.ckbFetchChildren.TabIndex = 28;
            this.ckbFetchChildren.Text = "Fetch Childen";
            this.ckbFetchChildren.UseVisualStyleBackColor = true;
            // 
            // nudLimit
            // 
            this.nudLimit.Location = new System.Drawing.Point(81, 121);
            this.nudLimit.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudLimit.Name = "nudLimit";
            this.nudLimit.Size = new System.Drawing.Size(76, 20);
            this.nudLimit.TabIndex = 27;
            this.nudLimit.Value = new decimal(new int[] {
            250,
            0,
            0,
            0});
            // 
            // lblLimit
            // 
            this.lblLimit.AutoSize = true;
            this.lblLimit.Location = new System.Drawing.Point(7, 123);
            this.lblLimit.Name = "lblLimit";
            this.lblLimit.Size = new System.Drawing.Size(31, 13);
            this.lblLimit.TabIndex = 26;
            this.lblLimit.Text = "Limit:";
            // 
            // nudOffset
            // 
            this.nudOffset.Location = new System.Drawing.Point(81, 95);
            this.nudOffset.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudOffset.Name = "nudOffset";
            this.nudOffset.Size = new System.Drawing.Size(76, 20);
            this.nudOffset.TabIndex = 25;
            // 
            // lblOffset
            // 
            this.lblOffset.AutoSize = true;
            this.lblOffset.Location = new System.Drawing.Point(7, 97);
            this.lblOffset.Name = "lblOffset";
            this.lblOffset.Size = new System.Drawing.Size(38, 13);
            this.lblOffset.TabIndex = 23;
            this.lblOffset.Text = "Offset:";
            // 
            // btnContainerPush
            // 
            this.btnContainerPush.Enabled = false;
            this.btnContainerPush.Image = global::jtl.ConnectorTester.Properties.Resources.share;
            this.btnContainerPush.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnContainerPush.Location = new System.Drawing.Point(10, 370);
            this.btnContainerPush.Name = "btnContainerPush";
            this.btnContainerPush.Size = new System.Drawing.Size(299, 43);
            this.btnContainerPush.TabIndex = 22;
            this.btnContainerPush.Text = "Model Push";
            this.btnContainerPush.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnContainerPush.UseVisualStyleBackColor = true;
            this.btnContainerPush.Click += new System.EventHandler(this.btnContainerPush_Click);
            // 
            // lblAction
            // 
            this.lblAction.AutoSize = true;
            this.lblAction.Location = new System.Drawing.Point(7, 45);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(40, 13);
            this.lblAction.TabIndex = 21;
            this.lblAction.Text = "Action:";
            // 
            // btnAction
            // 
            this.btnAction.Image = global::jtl.ConnectorTester.Properties.Resources.action;
            this.btnAction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAction.Location = new System.Drawing.Point(10, 419);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(299, 43);
            this.btnAction.TabIndex = 20;
            this.btnAction.Text = "Trigger Action";
            this.btnAction.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // cbAction
            // 
            this.cbAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAction.FormattingEnabled = true;
            this.cbAction.Location = new System.Drawing.Point(81, 42);
            this.cbAction.Name = "cbAction";
            this.cbAction.Size = new System.Drawing.Size(228, 21);
            this.cbAction.TabIndex = 19;
            this.cbAction.SelectedIndexChanged += new System.EventHandler(this.cbAction_SelectedIndexChanged);
            // 
            // lblWatch
            // 
            this.lblWatch.AutoSize = true;
            this.lblWatch.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWatch.Location = new System.Drawing.Point(77, 167);
            this.lblWatch.Name = "lblWatch";
            this.lblWatch.Size = new System.Drawing.Size(112, 21);
            this.lblWatch.TabIndex = 16;
            this.lblWatch.Text = "-- Milliseconds";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Anzahl:";
            // 
            // nudCount
            // 
            this.nudCount.Location = new System.Drawing.Point(81, 69);
            this.nudCount.Name = "nudCount";
            this.nudCount.Size = new System.Drawing.Size(163, 20);
            this.nudCount.TabIndex = 10;
            this.nudCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnDisconnect);
            this.groupBox2.Controls.Add(this.cboxUrl);
            this.groupBox2.Controls.Add(this.txtToken);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.btnSession);
            this.groupBox2.Location = new System.Drawing.Point(12, 77);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1244, 105);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Connector";
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDisconnect.Enabled = false;
            this.btnDisconnect.Image = global::jtl.ConnectorTester.Properties.Resources.disconnect;
            this.btnDisconnect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDisconnect.Location = new System.Drawing.Point(1124, 58);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(114, 38);
            this.btnDisconnect.TabIndex = 5;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // cboxUrl
            // 
            this.cboxUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxUrl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxUrl.FormattingEnabled = true;
            this.cboxUrl.Location = new System.Drawing.Point(60, 24);
            this.cboxUrl.Name = "cboxUrl";
            this.cboxUrl.Size = new System.Drawing.Size(1058, 21);
            this.cboxUrl.TabIndex = 4;
            this.cboxUrl.SelectedIndexChanged += new System.EventHandler(this.cboxUrl_SelectedIndexChanged);
            // 
            // txtToken
            // 
            this.txtToken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtToken.Location = new System.Drawing.Point(60, 51);
            this.txtToken.Name = "txtToken";
            this.txtToken.ReadOnly = true;
            this.txtToken.Size = new System.Drawing.Size(1058, 20);
            this.txtToken.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Token:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Url:";
            // 
            // btnSession
            // 
            this.btnSession.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSession.Image = global::jtl.ConnectorTester.Properties.Resources.auth;
            this.btnSession.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSession.Location = new System.Drawing.Point(1124, 14);
            this.btnSession.Name = "btnSession";
            this.btnSession.Size = new System.Drawing.Size(114, 38);
            this.btnSession.TabIndex = 0;
            this.btnSession.Text = "Authentication";
            this.btnSession.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSession.UseVisualStyleBackColor = true;
            this.btnSession.Click += new System.EventHandler(this.btnSession_Click);
            // 
            // tcResponse
            // 
            this.tcResponse.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tcResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcResponse.Controls.Add(this.tabPage1);
            this.tcResponse.Controls.Add(this.tabPage2);
            this.tcResponse.Controls.Add(this.tabPage3);
            this.tcResponse.Enabled = false;
            this.tcResponse.Location = new System.Drawing.Point(337, 188);
            this.tcResponse.Name = "tcResponse";
            this.tcResponse.SelectedIndex = 0;
            this.tcResponse.Size = new System.Drawing.Size(919, 564);
            this.tcResponse.TabIndex = 8;
            this.tcResponse.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.rtbResponse);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(911, 538);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Text";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tvResponse);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(911, 538);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tree";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tvResponse
            // 
            this.tvResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvResponse.Location = new System.Drawing.Point(0, 0);
            this.tvResponse.Name = "tvResponse";
            this.tvResponse.Size = new System.Drawing.Size(908, 535);
            this.tvResponse.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.webBrowser1);
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(911, 538);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "HTML";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(909, 535);
            this.webBrowser1.TabIndex = 0;
            // 
            // dialogHeader1
            // 
            this.dialogHeader1.BackColor = System.Drawing.Color.White;
            this.dialogHeader1.Description = "It will make you bulletproof";
            this.dialogHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dialogHeader1.Location = new System.Drawing.Point(0, 24);
            this.dialogHeader1.Margin = new System.Windows.Forms.Padding(0);
            this.dialogHeader1.Name = "dialogHeader1";
            this.dialogHeader1.Size = new System.Drawing.Size(1264, 50);
            this.dialogHeader1.TabIndex = 9;
            this.dialogHeader1.Text = "Connector Tester";
            this.dialogHeader1.TextImage = null;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1264, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.closeToolStripMenuItem.Text = "&Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.settingsToolStripMenuItem.Text = "&Settings...";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 761);
            this.Controls.Add(this.dialogHeader1);
            this.Controls.Add(this.tcResponse);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gbControl);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Connector Tester";
            this.gbControl.ResumeLayout(false);
            this.gbControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCount)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tcResponse.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSession;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtbResponse;
        private System.Windows.Forms.GroupBox gbControl;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudCount;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboxUrl;
        private System.Windows.Forms.Label lblWatch;
        private System.Windows.Forms.TabControl tcResponse;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TreeView tvResponse;
        private prounion.Controls.DialogHeader dialogHeader1;
        private System.Windows.Forms.ComboBox cbAction;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.Label lblAction;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Button btnContainerPush;
        private System.Windows.Forms.Label lblOffset;
        private System.Windows.Forms.NumericUpDown nudLimit;
        private System.Windows.Forms.Label lblLimit;
        private System.Windows.Forms.NumericUpDown nudOffset;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.TextBox txtParentId;
        private System.Windows.Forms.CheckBox ckbFetchChildren;
        private System.Windows.Forms.Button btnAckClear;
        private System.Windows.Forms.Button btnAck;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.Button btnTest;
    }
}

