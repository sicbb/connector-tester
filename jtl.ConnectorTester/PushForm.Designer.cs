﻿namespace jtl.ConnectorTester
{
    partial class PushForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dialogHeader1 = new prounion.Controls.DialogHeader();
            this.tcResponse = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.rtbResponse = new System.Windows.Forms.RichTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tvResponse = new System.Windows.Forms.TreeView();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.tcResponse2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.rtbResponse2 = new System.Windows.Forms.RichTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tvResponse2 = new System.Windows.Forms.TreeView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.btnClose = new System.Windows.Forms.Button();
            this.cmbAction = new System.Windows.Forms.ComboBox();
            this.lblAction = new System.Windows.Forms.Label();
            this.btnFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnDelete = new System.Windows.Forms.Button();
            this.dgvImages = new System.Windows.Forms.DataGridView();
            this.tcResponse.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tcResponse2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvImages)).BeginInit();
            this.SuspendLayout();
            // 
            // dialogHeader1
            // 
            this.dialogHeader1.BackColor = System.Drawing.Color.White;
            this.dialogHeader1.Description = null;
            this.dialogHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dialogHeader1.Location = new System.Drawing.Point(0, 0);
            this.dialogHeader1.Margin = new System.Windows.Forms.Padding(0);
            this.dialogHeader1.Name = "dialogHeader1";
            this.dialogHeader1.Size = new System.Drawing.Size(1195, 50);
            this.dialogHeader1.TabIndex = 0;
            this.dialogHeader1.Text = "dialogHeader1";
            this.dialogHeader1.TextImage = null;
            // 
            // tcResponse
            // 
            this.tcResponse.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tcResponse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcResponse.Controls.Add(this.tabPage1);
            this.tcResponse.Controls.Add(this.tabPage2);
            this.tcResponse.Location = new System.Drawing.Point(12, 53);
            this.tcResponse.Name = "tcResponse";
            this.tcResponse.SelectedIndex = 0;
            this.tcResponse.Size = new System.Drawing.Size(651, 432);
            this.tcResponse.TabIndex = 9;
            this.tcResponse.SelectedIndexChanged += new System.EventHandler(this.tcResponse_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.rtbResponse);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(643, 406);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Text";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // rtbResponse
            // 
            this.rtbResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbResponse.Location = new System.Drawing.Point(6, 6);
            this.rtbResponse.Name = "rtbResponse";
            this.rtbResponse.Size = new System.Drawing.Size(631, 394);
            this.rtbResponse.TabIndex = 5;
            this.rtbResponse.Text = "";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tvResponse);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(831, 406);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tree";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tvResponse
            // 
            this.tvResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvResponse.Location = new System.Drawing.Point(6, 6);
            this.tvResponse.Name = "tvResponse";
            this.tvResponse.Size = new System.Drawing.Size(819, 394);
            this.tvResponse.TabIndex = 0;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnUpdate.Image = global::jtl.ConnectorTester.Properties.Resources.share;
            this.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.Location = new System.Drawing.Point(1091, 469);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(87, 45);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Push";
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // tcResponse2
            // 
            this.tcResponse2.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tcResponse2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcResponse2.Controls.Add(this.tabPage3);
            this.tcResponse2.Controls.Add(this.tabPage4);
            this.tcResponse2.Controls.Add(this.tabPage5);
            this.tcResponse2.Location = new System.Drawing.Point(12, 520);
            this.tcResponse2.Name = "tcResponse2";
            this.tcResponse2.SelectedIndex = 0;
            this.tcResponse2.Size = new System.Drawing.Size(1170, 432);
            this.tcResponse2.TabIndex = 11;
            this.tcResponse2.SelectedIndexChanged += new System.EventHandler(this.tcResponse2_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.rtbResponse2);
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1162, 406);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Text";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // rtbResponse2
            // 
            this.rtbResponse2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbResponse2.Location = new System.Drawing.Point(6, 6);
            this.rtbResponse2.Name = "rtbResponse2";
            this.rtbResponse2.Size = new System.Drawing.Size(1150, 394);
            this.rtbResponse2.TabIndex = 5;
            this.rtbResponse2.Text = "";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tvResponse2);
            this.tabPage4.Location = new System.Drawing.Point(4, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(831, 406);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Tree";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tvResponse2
            // 
            this.tvResponse2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvResponse2.Location = new System.Drawing.Point(6, 6);
            this.tvResponse2.Name = "tvResponse2";
            this.tvResponse2.Size = new System.Drawing.Size(819, 394);
            this.tvResponse2.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.webBrowser1);
            this.tabPage5.Location = new System.Drawing.Point(4, 4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(831, 406);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "HTML";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(831, 406);
            this.webBrowser1.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(1103, 936);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cmbAction
            // 
            this.cmbAction.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAction.FormattingEnabled = true;
            this.cmbAction.Items.AddRange(new object[] {
            "complete",
            "insert",
            "update",
            "delete"});
            this.cmbAction.Location = new System.Drawing.Point(952, 482);
            this.cmbAction.Name = "cmbAction";
            this.cmbAction.Size = new System.Drawing.Size(133, 21);
            this.cmbAction.TabIndex = 13;
            this.cmbAction.SelectedIndexChanged += new System.EventHandler(this.cmbAction_SelectedIndexChanged);
            // 
            // lblAction
            // 
            this.lblAction.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblAction.AutoSize = true;
            this.lblAction.Location = new System.Drawing.Point(877, 485);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(69, 13);
            this.lblAction.TabIndex = 14;
            this.lblAction.Text = "Entity Action:";
            // 
            // btnFile
            // 
            this.btnFile.Enabled = false;
            this.btnFile.Image = global::jtl.ConnectorTester.Properties.Resources.openfolder;
            this.btnFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFile.Location = new System.Drawing.Point(740, 469);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(131, 43);
            this.btnFile.TabIndex = 15;
            this.btnFile.Text = "Add Image File";
            this.btnFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFile.UseVisualStyleBackColor = true;
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::jtl.ConnectorTester.Properties.Resources.stop;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(650, 469);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(84, 43);
            this.btnDelete.TabIndex = 16;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // dgvImages
            // 
            this.dgvImages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvImages.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvImages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvImages.Location = new System.Drawing.Point(669, 57);
            this.dgvImages.Name = "dgvImages";
            this.dgvImages.Size = new System.Drawing.Size(514, 400);
            this.dgvImages.TabIndex = 17;
            // 
            // PushForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1195, 968);
            this.Controls.Add(this.dgvImages);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnFile);
            this.Controls.Add(this.lblAction);
            this.Controls.Add(this.cmbAction);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.tcResponse2);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.tcResponse);
            this.Controls.Add(this.dialogHeader1);
            this.MaximizeBox = false;
            this.Name = "PushForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Container";
            this.Shown += new System.EventHandler(this.ContainerForm_Shown);
            this.tcResponse.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tcResponse2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvImages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private prounion.Controls.DialogHeader dialogHeader1;
        private System.Windows.Forms.TabControl tcResponse;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.RichTextBox rtbResponse;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TreeView tvResponse;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TabControl tcResponse2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RichTextBox rtbResponse2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TreeView tvResponse2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.ComboBox cmbAction;
        private System.Windows.Forms.Label lblAction;
        private System.Windows.Forms.Button btnFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridView dgvImages;
    }
}