﻿using System;
using System.Reactive.Linq;
using System.Windows.Forms;
using jtl.ConnectorTester.Core.Mock;

namespace jtl.ConnectorTester
{
    public partial class PushTestForm : Form
    {
        private readonly Mocker _mocker;

        public PushTestForm(string sessionId, string url)
        {
            InitializeComponent();
            _mocker = new Mocker(sessionId, url);
            _mocker.Connector.JsonRequest.ObserveOn(this).Subscribe(str =>
            {
                rtbRpc.Text += string.Format("#########{0}", Environment.NewLine);
                rtbRpc.Text += string.Format("# Request #{0}", Environment.NewLine);
                rtbRpc.Text += string.Format("#########{0}", Environment.NewLine);
                rtbRpc.Text += string.Format("{0}{1}{2}", str, Environment.NewLine, Environment.NewLine);

            });

            _mocker.Connector.JsonResponse.ObserveOn(this).Subscribe(str =>
            {
                rtbRpc.Text += string.Format("##########{0}", Environment.NewLine);
                rtbRpc.Text += string.Format("# Response #{0}", Environment.NewLine);
                rtbRpc.Text += string.Format("##########{0}", Environment.NewLine);
                rtbRpc.Text += string.Format("{0}{1}{2}", str, Environment.NewLine, Environment.NewLine);
            });

            _mocker.Connector.JsonError.ObserveOn(this).Subscribe(str =>
            {
                rtbError.Text += string.Format("######{0}", Environment.NewLine);
                rtbError.Text += string.Format("# Error #{0}", Environment.NewLine);
                rtbError.Text += string.Format("######{0}", Environment.NewLine);
                rtbError.Text += string.Format("{0}{1}{2}", str, Environment.NewLine, Environment.NewLine);
            });
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            rtbRpc.Text = string.Empty;
            rtbError.Text = string.Empty;
            _mocker.Run();
        }
    }
}
