﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using jtl.ConnectorTester.Core.Model;

namespace jtl.ConnectorTester
{
    public partial class Settings : Form
    {
        public Setting Setting { get; protected set; }

        public Settings()
        {
            InitializeComponent();

            Setting = new Setting();
            Setting.Load();

            nudWidth.Text = Setting.Resolution.Width.ToString(CultureInfo.InvariantCulture);
            nudHeight.Text = Setting.Resolution.Height.ToString(CultureInfo.InvariantCulture);
            dgvEndpointList.DataSource = new BindingSource { DataSource = Setting.Endpoints };
            dgvEndpointList.FirstDisplayedScrollingRowIndex = Setting.Endpoints.Count == 0 ? 0 : Setting.Endpoints.Count - 1;
            cbEndpoint.DataSource = new BindingSource { DataSource = Setting.Endpoints };
            if (Setting.EndpointIndex <= (cbEndpoint.Items.Count - 1))
            {
                cbEndpoint.SelectedIndex = Setting.EndpointIndex;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Setting.Resolution.Width = Convert.ToInt32(nudWidth.Text);
            Setting.Resolution.Height = Convert.ToInt32(nudHeight.Text);
            Setting.EndpointIndex = cbEndpoint.SelectedIndex;
            Setting.Save();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void dgvEndpointList_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Setting.Endpoints = (List<Endpoint>) ((BindingSource) dgvEndpointList.DataSource).DataSource;
            cbEndpoint.DataSource = new BindingSource { DataSource = Setting.Endpoints };
        }

        private void dgvEndpointList_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            Setting.Endpoints = (List<Endpoint>)((BindingSource)dgvEndpointList.DataSource).DataSource;
            cbEndpoint.DataSource = new BindingSource { DataSource = Setting.Endpoints };
        }
    }
}
