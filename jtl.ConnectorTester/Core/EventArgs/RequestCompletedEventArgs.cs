﻿using System.Net;

namespace jtl.ConnectorTester.Core.EventArgs
{
    public class RequestCompletedEventArgs : System.EventArgs
    {
        public HttpWebResponse WebResponse { get; protected set; }

        public RequestCompletedEventArgs(HttpWebResponse response)
        {
            WebResponse = response;
        }
    }
}
