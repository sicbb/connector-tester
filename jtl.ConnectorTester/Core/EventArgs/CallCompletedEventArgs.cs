﻿using jtl.ConnectorTester.Core.Rpc;

namespace jtl.ConnectorTester.Core.EventArgs
{
    public class CallCompletedEventArgs : System.EventArgs
    {
        public RpcResponse Response { get; protected set; }

        public CallCompletedEventArgs(RpcResponse response)
        {
            Response = response;
        }
    }
}
