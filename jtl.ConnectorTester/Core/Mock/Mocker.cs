﻿using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using jtl.ConnectorTester.Core.Model;
using jtl.ConnectorTester.Model;
using jtl.ConnectorTester.Properties;
using Newtonsoft.Json;
using Image = System.Drawing.Image;

namespace jtl.ConnectorTester.Core.Mock
{
    public class Mocker
    {
        public readonly Connector Connector;
        private readonly string _url;
        private Features _features;

        public Mocker(string sessionId, string url)
        {
            Connector = new Connector { SessionId = sessionId };
            _url = url;
        }

        public Task Run()
        {
            return Task.Run(() => RunIntern());
        }

        private void RunIntern()
        {
            // Feature Matrix
            _features = PullFeatures();

            // Push Manufacturer
            if (_features.Entities["Manufacturer"].Push)
            {
                Connector.PushList(_url, "Manufacturer", JsonConvert.DeserializeObject<List<ManufacturerModel>>(Resources.ManufacturerMock));
            }

            // Push Category
            if (_features.Entities["Category"].Push)
            {
                Connector.PushList(_url, "Category", JsonConvert.DeserializeObject<List<CategoryModel>>(Resources.CategoryMock));
            }

            // Push Products
            if (_features.Entities["Product"].Push)
            {
                if ((bool)_features.Flags[Features.VariationCombinationsSupported])
                {
                    if ((bool)_features.Flags[Features.VarCombinationChildFirst])
                    {
                        Connector.PushList(_url, "Product",
                            JsonConvert.DeserializeObject<List<ProductModel>>(Resources.ProductMockChild));
                    }
                    else
                    {
                        Connector.PushList(_url, "Product",
                            JsonConvert.DeserializeObject<List<ProductModel>>(Resources.ProductMock));
                    }
                }
                else if ((bool)_features.Flags[Features.VariationProductsSupported])
                {
                    Connector.PushList(_url, "Product",
                            JsonConvert.DeserializeObject<List<ProductModel>>(Resources.ProductMockVar));
                }
            }

            // Push Images
            if ((bool)_features.Flags[Features.CategoryImagesSupported])
            {
                Connector.Picture = ImageToByte(Resources.apple_logo);
                Connector.Picturename = "Apple_logo.jpg";
                Connector.Push(_url, "Image", JsonConvert.DeserializeObject<ImageModel>(Resources.ImageMockApple), false);
            }

            if ((bool)_features.Flags[Features.ProductImagesSupported])
            {
                if (!(bool)_features.Flags[Features.VariationCombinationsSupported])
                {
                    Connector.Picture = ImageToByte(Resources.iphone6_grey);
                    Connector.Picturename = "iPhone6_grey.png";
                    Connector.Push(_url, "Image", JsonConvert.DeserializeObject<ImageModel>(Resources.ImageMockPhone),
                        false);
                }
                else
                {
                    Connector.Picture = ImageToByte(Resources.iphone6_grey);
                    Connector.Picturename = "iPhone6_grey.png";
                    Connector.Push(_url, "Image", JsonConvert.DeserializeObject<ImageModel>(Resources.ImageMockPhoneGrey),
                        false);

                    Connector.Picture = ImageToByte(Resources.iphone6_gold);
                    Connector.Picturename = "iPhone6_gold.png";
                    Connector.Push(_url, "Image", JsonConvert.DeserializeObject<ImageModel>(Resources.ImageMockPhoneGold),
                        false);
                }

                Connector.Picture = ImageToByte(Resources.ladekabel);
                Connector.Picturename = "Ladekabel.jpg";
                Connector.Push(_url, "Image", JsonConvert.DeserializeObject<ImageModel>(Resources.ImageMockCable), false);
            }
        }

        private Features PullFeatures()
        {
            var response = Connector.GetFeatureMatrix(_url);

            return JsonConvert.DeserializeObject<Features>(response.Result.ToString());
        }

        private byte[] ImageToByte(Image img)
        {
            var converter = new ImageConverter();

            return (byte[]) converter.ConvertTo(img, typeof (byte[]));
        }
    }
}
