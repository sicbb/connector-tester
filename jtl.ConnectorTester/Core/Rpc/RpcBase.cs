﻿using Newtonsoft.Json;

namespace jtl.ConnectorTester.Core.Rpc
{
    public class RpcBase : System.EventArgs
    {
        [JsonProperty(Required = Required.Always, PropertyName = "jtlrpc")]
        public string Jtlrpc { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "id")]
        public string Id { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
