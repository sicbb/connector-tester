﻿using Newtonsoft.Json;

namespace jtl.ConnectorTester.Core.Rpc
{
    public class RpcResponse : RpcBase
    {
        [JsonProperty(Required = Required.AllowNull, PropertyName = "result")]
        public object Result { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "error")]
        public Error Error { get; set; }
    }
}
