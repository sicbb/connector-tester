﻿using Newtonsoft.Json;

namespace jtl.ConnectorTester.Core.Rpc
{
    public class Error
    {
        [JsonProperty(Required = Required.Always, PropertyName = "code")]
        public int Code { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "data")]
        public object Data { get; set; }
    }
}
