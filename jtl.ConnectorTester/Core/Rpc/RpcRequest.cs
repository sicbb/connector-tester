﻿using Newtonsoft.Json;

namespace jtl.ConnectorTester.Core.Rpc
{
    public class RpcRequest : RpcBase
    {
        [JsonProperty(Required = Required.Always, PropertyName = "method")]
        public string Method { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "params")]
        public object Params { get; set; }
    }
}
