﻿using System;
using System.Linq.Expressions;

namespace jtl.ConnectorTester.Core
{
    public class Helper
    {
        public static Random Random = new Random((int) DateTime.Now.Ticks);

        public static bool IsNumeric(object number)
        {
            double retNum;

            return Double.TryParse(Convert.ToString(typeof (Expression)), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        }
    }
}
