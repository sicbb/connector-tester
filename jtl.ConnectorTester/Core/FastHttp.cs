﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using jtl.ConnectorTester.Core.EventArgs;

namespace jtl.ConnectorTester.Core
{
    public class FastHttp
    {
        public event EventHandler<RequestCompletedEventArgs> RequestCompletedEvent;

        public enum RequestMethod { Get, Post };
        public string Content { get; protected set; }
        public bool Mobile { get; set; }
        public bool KeepAlive { get; set; }
        public const string NewLine = "\r\n";
        public HttpStatusCode StatusCode { get; protected set; }
        public NameValueCollection QueryParam { get; protected set; }
        public WebProxy Proxy { get; set; }
        public Encoding Encoding { get; set; }

        public FastHttp()
        {
            #region ServicePointManager
            WebRequest.DefaultWebProxy = null;
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.MaxServicePointIdleTime = 2000;
            ServicePointManager.UseNagleAlgorithm = false;
            #endregion

            Content = null;
            Mobile = false;
            KeepAlive = true;
            Encoding = Encoding.UTF8;
            StatusCode = HttpStatusCode.Unused;
            QueryParam = new NameValueCollection();
        }

        protected HttpWebRequest CreateRequest(string url, RequestMethod method, NetworkCredential credentials)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            request.Proxy = Proxy;
            request.Method = method.ToString();
            request.ProtocolVersion = HttpVersion.Version11;

            request.Timeout = 90000;
            request.KeepAlive = KeepAlive;
            request.AllowAutoRedirect = true;
            request.MaximumAutomaticRedirections = 3;
            request.AuthenticationLevel = AuthenticationLevel.None;

            if (credentials != null)
                request.Credentials = credentials;

            if (Mobile)
                request.UserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) CriOS/26.0.1410.53 Mobile/10B329 Safari/8536.25";

            request.Headers.Add(HttpRequestHeader.AcceptCharset, Encoding.BodyName);
            request.Headers.Add(HttpRequestHeader.CacheControl, "no-cache");
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");

            return request;
        }

        public void RequestAsync(string url, RequestMethod method = RequestMethod.Get)
        {
            Task.Run(() => Request(url, method));
        }

        public void UploadFileAsync(string url, string filename, byte[] content)
        {
            Task.Run(() => UploadFile(url, filename, content));
        }

        public void Request(string url, RequestMethod method = RequestMethod.Get, NetworkCredential credentials = null)
        {
            var request = CreateRequest(url, method, credentials);

            if (method == RequestMethod.Post)
            {
                var queryString = ToQueryString(QueryParam);

                var byteQueryString = Encoding.GetBytes(queryString);
                request.ContentLength = byteQueryString.Length;

                request.ContentType = "application/x-www-form-urlencoded";

                using (var postStream = request.GetRequestStream())
                {
                    postStream.Write(byteQueryString, 0, byteQueryString.Length);
                }
            }

            request.BeginGetResponse(result =>
            {
                try
                {
                    var httpWebRequest = result.AsyncState as HttpWebRequest;
                    if (httpWebRequest == null) return;

                    var response = (HttpWebResponse) httpWebRequest.EndGetResponse(result);
                    ReadResponse(response);
                    OnRequestCompletedEvent(this, new RequestCompletedEventArgs(response));
                }
                catch (WebException e)
                {
                    var response = (HttpWebResponse)e.Response;
                    ReadResponse(response);
                    OnRequestCompletedEvent(this, new RequestCompletedEventArgs(response));
                }
            }, request);
        }

        protected void ReadResponse(HttpWebResponse response)
        {
            using (response)
            {
                StatusCode = response.StatusCode;
                var stream = response.GetResponseStream();
                var encoding = response.ContentEncoding.ToLower();

                if (stream == null) return;

                switch (encoding)
                {
                    case "gzip":
                        stream = new GZipStream(stream, CompressionMode.Decompress);
                        break;

                    case "deflate":
                        stream = new DeflateStream(stream, CompressionMode.Decompress);
                        break;
                }

                using (var reader = new StreamReader(stream, Encoding.Default))
                {
                    Content = reader.ReadToEnd();
                    Content = Content.Trim();
                }

                stream.Dispose();
            }
        }

        public void UploadFile(string url, string filename, byte[] content)
        {
            var request = CreateRequest(url, RequestMethod.Post, null);

            var boundary = CreateBoundary();
            var boundarybytes = Encoding.ASCII.GetBytes(
                string.Format("{0}--{1}{0}", NewLine, boundary));

            request.ContentType = "multipart/form-data; boundary=" + boundary;

            using (var rs = request.GetRequestStream())
            {
                const string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"{2}{2}{1}";

                foreach (string key in QueryParam.Keys)
                {
                    rs.Write(boundarybytes, 0, boundarybytes.Length);
                    var formitem = string.Format(formdataTemplate, key, QueryParam[key], NewLine);
                    var formitembytes = Encoding.UTF8.GetBytes(formitem);
                    rs.Write(formitembytes, 0, formitembytes.Length);
                }

                rs.Write(boundarybytes, 0, boundarybytes.Length);

                const string headerTemplate = "Content-Disposition: form-data; name=\"file\"; filename=\"{0}\"{2}Content-Type: {1}{2}{2}";

                var header = string.Format(headerTemplate, filename, "application/octet-stream", NewLine);
                var headerbytes = Encoding.UTF8.GetBytes(header);

                rs.Write(headerbytes, 0, headerbytes.Length);
                rs.Write(content, 0, content.Length);

                var trailer = Encoding.ASCII.GetBytes(string.Format("{1}--{0}--{1}", boundary, NewLine));

                rs.Write(trailer, 0, trailer.Length);
            }

            request.BeginGetResponse(result =>
            {
                try
                {
                    var httpWebRequest = result.AsyncState as HttpWebRequest;
                    if (httpWebRequest == null) return;

                    var response = (HttpWebResponse) httpWebRequest.EndGetResponse(result);
                    ReadResponse(response);
                    OnRequestCompletedEvent(this, new RequestCompletedEventArgs(response));
                }
                catch (WebException e)
                {
                    var response = (HttpWebResponse) e.Response;
                    ReadResponse(response);
                    OnRequestCompletedEvent(this, new RequestCompletedEventArgs(response));
                }
            }, request);
        }

        /// <summary>
        /// http://www.w3.org/Protocols/rfc1341/7_2_Multipart.html
        /// </summary>
        /// <returns></returns>
        protected string CreateBoundary()
        {
            return "---------------------------" + DateTime.Now.Ticks.ToString("x");
        }

        public class Bandwidth
        {
            public int UploadBytesPerSecond { get; set; }
            public int DownloadBytesPerSecond { get; set; }

            public Bandwidth()
            {
                UploadBytesPerSecond = 0;
                DownloadBytesPerSecond = 0;
            }
        }

        protected string ToQueryString(NameValueCollection query)
        {
            if (query.Count > 0)
            {
                return string.Join("&", Array.ConvertAll(query.AllKeys,
                    key => string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(query[key])))
                );
            }

            return "";
        }

        protected virtual void OnRequestCompletedEvent(object sender, RequestCompletedEventArgs e)
        {
            var del = RequestCompletedEvent;
            if (del != null)
            {
                del(sender, e);
            }
        }
    }
}
