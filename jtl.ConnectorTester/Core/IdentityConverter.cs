﻿using System;
using jtl.ConnectorTester.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace jtl.ConnectorTester.Core
{
    public class IdentityConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(Identity));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var identity = new Identity();

            if (reader.TokenType != JsonToken.StartArray)
                return identity;

            var list = serializer.Deserialize<JArray>(reader);

            if (list == null || list.Count != 2)
                return identity;

            identity.Set(list[0], list[1]);

            return identity;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (!(value is Identity))
                return;

            var identity = value as Identity;
            var array = new JArray(identity.Endpoint, identity.Host);

            array.WriteTo(writer);
        }
    }
}
