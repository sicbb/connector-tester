﻿namespace jtl.ConnectorTester.Core.Model
{
    public class Resolution
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
