﻿using jtl.ConnectorTester.Model;

namespace jtl.ConnectorTester.Core.Model
{
    public class Image
    {
        public int HostId { get; set; }
        public ImageRelationTypeModel RelationType { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }

        public Image()
        {
            RelationType = ImageRelationTypeModel.Product;
        }
    }
}
