﻿namespace jtl.ConnectorTester.Core.Model
{
    public class Endpoint
    {
        public string Url { get; set; }
        public string Token { get; set; }

        public Endpoint()
        {
            Url = string.Empty;
            Token = string.Empty;
        }

        public override string ToString()
        {
            return Url;
        }
    }
}
