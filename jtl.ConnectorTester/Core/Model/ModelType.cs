﻿using System.Runtime.Serialization;

namespace jtl.ConnectorTester.Core.Model
{
    public enum ModelType
    {
        [EnumMember(Value = "Company")]
        Company,
        [EnumMember(Value = "CrossSellingGroup")]
        CrossSellingGroup,
        [EnumMember(Value = "CrossSelling")]
        CrossSelling,
        [EnumMember(Value = "Currency")]
        Currency,
        [EnumMember(Value = "CustomerGroup")]
        CustomerGroup,
        [EnumMember(Value = "Language")]
        Language,
        [EnumMember(Value = "ShippingClass")]
        ShippingClass,
        [EnumMember(Value = "TaxClass")]
        TaxClass,
        [EnumMember(Value = "TaxRate")]
        TaxRate,
        [EnumMember(Value = "TaxZone")]
        TaxZone,
        [EnumMember(Value = "Unit")]
        Unit,
        [EnumMember(Value = "Warehouse")]
        Warehouse,
        [EnumMember(Value = "MeasurementUnit")]
        MeasurementUnit,
        [EnumMember(Value = "ProductType")]
        ProductType,
        [EnumMember(Value = "Product")]
        Product,
        [EnumMember(Value = "Specific")]
        Specific,
        [EnumMember(Value = "Category")]
        Category,
        [EnumMember(Value = "Customer")]
        Customer,
        [EnumMember(Value = "CustomerOrder")]
        CustomerOrder,
        [EnumMember(Value = "DeliveryNote")]
        DeliveryNote,
        [EnumMember(Value = "Manufacturer")]
        Manufacturer,
        [EnumMember(Value = "Image")]
        Image,
        [EnumMember(Value = "Payment")]
        Payment
    }
}
