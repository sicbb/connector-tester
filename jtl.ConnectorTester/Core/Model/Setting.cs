﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Core.Model
{
    public class Setting
    {
        [JsonIgnore]
        public string JsonFolder { get; set; }

        [JsonIgnore]
        public string JsonPath { get; set; }

        public Resolution Resolution { get; set; }
        public int EndpointIndex { get; set; }
        public List<Endpoint> Endpoints { get; set; }

        public Setting()
        {
            JsonFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Jtl", "ConnectorTester");
            JsonPath = Path.Combine(JsonFolder, "setting.json");

            if (!Directory.Exists(JsonFolder))
            {
                Directory.CreateDirectory(JsonFolder);
            }

            Resolution = new Resolution();
            EndpointIndex = 0;
            Endpoints = new List<Endpoint>();
        }

        public void Load()
        {
            if (!File.Exists(JsonPath)) return;

            var obj = JsonConvert.DeserializeObject<Setting>(File.ReadAllText(JsonPath));
            Resolution = obj.Resolution;
            EndpointIndex = obj.EndpointIndex;
            Endpoints = obj.Endpoints;
        }

        public void Save()
        {
            File.WriteAllText(JsonPath, JsonConvert.SerializeObject(this));
        }
    }
}
