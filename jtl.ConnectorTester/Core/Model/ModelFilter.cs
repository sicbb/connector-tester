﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace jtl.ConnectorTester.Core.Model
{
    public class ModelFilter
    {
        [JsonProperty(Required = Required.Always, PropertyName = "limit")]
        public int Limit { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "offset")]
        public int Offset { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "from")]
        public DateTime? From { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "until")]
        public DateTime? Until { get; set; }

        [JsonProperty(Required = Required.AllowNull, PropertyName = "filters")]
        public Dictionary<string, object> Filters { get; set; }

        public ModelFilter()
        {
            Filters = new Dictionary<string, object>();
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
