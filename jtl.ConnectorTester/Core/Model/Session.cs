﻿using Newtonsoft.Json;

namespace jtl.ConnectorTester.Core.Model
{
    public class Session
    {
        [JsonProperty(Required = Required.Always, PropertyName = "sessionId")]
        public string SessionId { get; set; }

        [JsonProperty(Required = Required.Always, PropertyName = "lifetime")]
        public string Lifetime { get; set; }
    }
}
