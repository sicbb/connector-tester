﻿using System.Collections;
using System.Globalization;
using jtl.ConnectorTester.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jtl.ConnectorTester.Core.Model
{
    public abstract class MainModel
    {
        //[JsonProperty(Required = Required.AllowNull, PropertyName = "action")]
        //public string Action { get; set; }

        /*
        public void ActionChange(string action)
        {
            var t = GetType();
            var properties = t.GetProperties();

            foreach (var property in properties)
            {
                if (property.GetValue(this) is IList)
                {
                    var list = (IList)property.GetValue(this);
                    foreach (MainModel item in list)
                    {
                        item.ActionChange(action);
                    }
                }
            }

            Action = action;
        }
        */

        public static List<string> MainList = new List<string>
        {
            "Product",
            "Category",
            "Customer",
            "CustomerOrder",
            "DeliveryNote",
            "GlobalData",
            "Image",
            "Manufacturer",
            "Specific",
            "Connector",
            "StatusChange",
            "ProductStockLevel",
            "ProductPrice",
            "Payment",
            "CrossSelling"
        };

        public void Fill()
        {
            var type = GetType();
            var propertyInfos = type.GetProperties();

            foreach (var propertyInfo in propertyInfos)
            {
                switch (propertyInfo.PropertyType.Name)
                {
                    case "Int32":
                        propertyInfo.SetValue(this, Helper.Random.Next(1, 100));
                        //propertyInfo.SetValue(this, 34);

                        if (propertyInfo.Name == "MasterImageId")
                            propertyInfo.SetValue(this, 0);
                        break;
                    case "String":
                        if (propertyInfo.Name == "LocaleName")
                            propertyInfo.SetValue(this, "de_DE");
                        else if (CheckDate(propertyInfo.Name))
                            propertyInfo.SetValue(this, DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"));
                        else if (propertyInfo.Name == "RelationType")
                            propertyInfo.SetValue(this, "product");
                        else if (propertyInfo.Name.ToLower().IndexOf("id", StringComparison.Ordinal) > -1)
                            propertyInfo.SetValue(this, Helper.Random.Next(1, 100).ToString(CultureInfo.InvariantCulture));
                        else
                            propertyInfo.SetValue(this, RandomString(15));
                        break;
                    case "Double":
                        propertyInfo.SetValue(this, Helper.Random.NextDouble() * (50.5 - 0.1) + 0.1);
                        break;
                    case "Boolean":
                        var value = Helper.Random.Next(0, 2) == 0;
                        propertyInfo.SetValue(this, value);
                        break;
                    case "Identity":
                        var identity = new Identity();
                        identity.Set(Helper.Random.Next(1, 100).ToString(CultureInfo.InvariantCulture), 0);
                        propertyInfo.SetValue(this, identity);
                        if (propertyInfo.Name == "MasterImageId")
                        {
                            propertyInfo.SetValue(this, new Identity());
                        }
                        break;
                }
            }
        }

        protected bool CheckDate(string fieldName)
        {
            string[] tmp = {
                "Created",
                "Birthday",
                "Modified",
                "ActiveFrom",
                "ActiveUntil",
                "BestBefore",
                "ShippingDate",
                "PaymentDate",
                "RatingNotificationDate",
                "AvailableFrom",
                "InflowDate"
            };

            var fields = tmp.ToList();

            return fields.Contains(fieldName);
        }

        public static Type GetType(string modelType)
        {
            return Type.GetType(String.Format("jtl.ConnectorTester.Model.{0}Model", modelType));
        }

        public static MainModel CreateObject(string modelType)
        {
            var type = Type.GetType(String.Format("jtl.ConnectorTester.Model.{0}Model", modelType));

            if (type == null)
            {
                return null;
            }

            return (MainModel) Activator.CreateInstance(type);
        }

        public static object Deserialize(string modelType, string json)
        {
            return JsonConvert.DeserializeObject(json, GetType(modelType));
        }

        public static IEnumerable DeserializeList(string modelType, string json)
        {
            switch (modelType)
            {
                case "Product":
                    return JsonConvert.DeserializeObject<List<ProductModel>>(json);
                case "Category":
                    return JsonConvert.DeserializeObject<List<CategoryModel>>(json);
                case "Customer":
                    return JsonConvert.DeserializeObject<List<CustomerModel>>(json);
                case "CustomerOrder":
                    return JsonConvert.DeserializeObject<List<CustomerOrderModel>>(json);
                case "DeliveryNote":
                    return JsonConvert.DeserializeObject<List<DeliveryNoteModel>>(json);
                case "GlobalData":
                    return JsonConvert.DeserializeObject<List<GlobalDataModel>>(json);
                case "Image":
                    return JsonConvert.DeserializeObject<List<ImageModel>>(json);
                case "Manufacturer":
                    return JsonConvert.DeserializeObject<List<ManufacturerModel>>(json);
                case "Specific":
                    return JsonConvert.DeserializeObject<List<SpecificModel>>(json);
                case "StatusChange":
                    return JsonConvert.DeserializeObject<List<StatusChangeModel>>(json);
                case "ProductStockLevel":
                    return JsonConvert.DeserializeObject<List<ProductStockLevelModel>>(json);
                case "ProductPrice":
                    return JsonConvert.DeserializeObject<List<ProductPriceModel>>(json);
                case "Payment":
                    return JsonConvert.DeserializeObject<List<PaymentModel>>(json);
                case "CrossSelling":
                    return JsonConvert.DeserializeObject<List<CrossSellingModel>>(json);
                case "Ack":
                    return JsonConvert.DeserializeObject<List<AckModel>>(json);
            }

            return null;
        }

        public static MainModel Deserialize(string modelType, string json, bool multiple)
        {
            switch (modelType)
            {
                case "Product":
                    return multiple ? JsonConvert.DeserializeObject<List<ProductModel>>(json)[0] : JsonConvert.DeserializeObject<ProductModel>(json);
                case "Category":
                    return multiple ? JsonConvert.DeserializeObject<List<CategoryModel>>(json)[0] : JsonConvert.DeserializeObject<CategoryModel>(json);
                case "Customer":
                    return multiple ? JsonConvert.DeserializeObject<List<CustomerModel>>(json)[0] : JsonConvert.DeserializeObject<CustomerModel>(json);
                case "CustomerOrder":
                    return multiple ? JsonConvert.DeserializeObject<List<CustomerOrderModel>>(json)[0] : JsonConvert.DeserializeObject<CustomerOrderModel>(json);
                case "DeliveryNote":
                    return multiple ? JsonConvert.DeserializeObject<List<DeliveryNoteModel>>(json)[0] : JsonConvert.DeserializeObject<DeliveryNoteModel>(json);
                case "GlobalData":
                    return multiple ? JsonConvert.DeserializeObject<List<GlobalDataModel>>(json)[0] : JsonConvert.DeserializeObject<GlobalDataModel>(json);
                case "Image":
                    return multiple ? JsonConvert.DeserializeObject<List<ImageModel>>(json)[0] : JsonConvert.DeserializeObject<ImageModel>(json);
                case "Manufacturer":
                    return multiple ? JsonConvert.DeserializeObject<List<ManufacturerModel>>(json)[0] : JsonConvert.DeserializeObject<ManufacturerModel>(json);
                case "Specific":
                    return multiple ? JsonConvert.DeserializeObject<List<SpecificModel>>(json)[0] : JsonConvert.DeserializeObject<SpecificModel>(json);
                case "StatusChange":
                    return multiple ? JsonConvert.DeserializeObject<List<StatusChangeModel>>(json)[0] : JsonConvert.DeserializeObject<StatusChangeModel>(json);
                case "ProductStockLevel":
                    return multiple ? JsonConvert.DeserializeObject<List<ProductStockLevelModel>>(json)[0] : JsonConvert.DeserializeObject<ProductStockLevelModel>(json);
                case "ProductPrice":
                    return multiple ? JsonConvert.DeserializeObject<List<ProductPriceModel>>(json)[0] : JsonConvert.DeserializeObject<ProductPriceModel>(json);
                case "Payment":
                    return multiple ? JsonConvert.DeserializeObject<List<PaymentModel>>(json)[0] : JsonConvert.DeserializeObject<PaymentModel>(json);
                case "CrossSelling":
                    return multiple ? JsonConvert.DeserializeObject<List<CrossSellingModel>>(json)[0] : JsonConvert.DeserializeObject<CrossSellingModel>(json);
            }

            return null;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public static string RandomString(int size)
        {
            var builder = new StringBuilder();
            for (var i = 0; i < size; i++)
            {
                var ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * Helper.Random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}
