﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace jtl.ConnectorTester.Core.Model
{
    [JsonObject(MemberSerialization.OptOut, ItemRequired = Required.Always)]
    public class EntitySupport
    {
        [JsonProperty("push")]
        public bool Push { get; set; }

        [JsonProperty("pull")]
        public bool Pull { get; set; }

        [JsonProperty("delete")]
        public bool Delete { get; set; }

        public EntitySupport()
        {
            Push = false;
            Pull = false;
            Delete = false;
        }
    }

    [JsonObject(MemberSerialization.OptIn, ItemRequired = Required.Always)]
    public class Features
    {
        /// <summary>
        /// Legt fest, ob der Endpoint zuerst Varkombi-Väter oder -Kinder erwartet (Standard: Väter zuerst)
        /// </summary>
        public const string VarCombinationChildFirst = @"var_combination_child_first";
        /// <summary>
        /// Bestimmt, ob Bestellungen nach Storno wieder in Kraft gesetzt werden können oder nur durch Neuanlage wieder eröffnet werden können (Standard: ja)
        /// </summary>
        public const string CanReactivateCustomerOrder = @"can_reactivate_customer_order";
        /// <summary>
        /// Bestimmt, ob Bestellpositionen nach Erfassung der Bestellung noch geändert werden können oder ob Bestellungen grundsätzlich fest sind (Standard: ja)
        /// </summary>
        public const string CanUpdateCustomerOrder = @"can_update_customer_order";
        /// <summary>
        /// Bestimmt, ob Teilzahlungen nach Art der Wawi mit dem Endpoint realisierbar sind (Standard: nein)
        /// </summary>
        public const string CustomerOrderPartiallyPayable = @"customer_order_partially_payable";
        /// <summary>
        /// Bestimmt, ob der Endpoint Produktbilder unterstützt (Standard: ja)
        /// </summary>
        public const string ProductImagesSupported = @"product_images_supported";
        /// <summary>
        /// Bestimmt, ob der Endpoint Kategoriebilder unterstützt (Standard: ja)
        /// </summary>
        public const string CategoryImagesSupported = @"category_images_supported";
        /// <summary>
        /// Bestimmt, ob der Endpoint Herstellerbilder unterstützt (Standard: nein)
        /// </summary>
        public const string ManufacturerImagesSupported = @"manufacturer_images_supported";
        /// <summary>
        /// Bestimmt, ob der Endpoint Merkmalbilder unterstützt (Standard: nein)
        /// </summary>
        public const string SpecificImagesSupported = @"specific_images_supported";
        /// <summary>
        /// Bestimmt, ob der Endpoint Bilder zu Merkmalwerten unterstützt (Standard: nein)
        /// </summary>
        public const string SpecificValueImagesSupported = @"specific_value_images_supported";
        /// <summary>
        /// Bestimmt, ob der Endpoint Bilder für Konfiguratorgruppen unterstützt (Standard: nein)
        /// </summary>
        public const string ConfigGroupImagesSupported = @"config_group_images_supported";
        /// <summary>
        /// Bestimmt, ob der Endpoint Bilder für Variationswerte unterstützt (Standard: nein)
        /// </summary>
        public const string ProductVariationValueImagesSupported = @"product_variation_value_images_supported";
        /// <summary>
        /// Bestimmt, ob der Endpoint Variationsartikel unterstützt (Standard: nein)
        /// </summary>
        public const string VariationProductsSupported = @"variation_products_supported";
        /// <summary>
        /// Bestimmt, ob der Endpoint Variationskombinationen unterstützt (Standard: nein)
        /// </summary>
        public const string VariationCombinationsSupported = @"variation_combinations_supported";
        /// <summary>
        /// Bestimmt, ob der Endpoint Stücklistenartikel unterstützt (Standard: nein)
        /// </summary>
        public const string SetArticlesSupported = @"set_articles_supported";

        [JsonProperty("entities")]
        public IDictionary<string, EntitySupport> Entities = new Dictionary<string, EntitySupport>();

        [JsonProperty("flags", Required = Required.AllowNull)]
        public IDictionary<string, object> Flags = DefaultFlags;

        public static Dictionary<string, object> DefaultFlags
        {
            get
            {
                return new Dictionary<string, object>
                {
                    { VarCombinationChildFirst, false},
                    { CanReactivateCustomerOrder, true},
                    { CanUpdateCustomerOrder, true},
                    { CustomerOrderPartiallyPayable, false},
                    { ProductImagesSupported, true},
                    { CategoryImagesSupported, true},
                    { ManufacturerImagesSupported, false},
                    { SpecificImagesSupported, false},
                    { SpecificValueImagesSupported, false},
                    { ConfigGroupImagesSupported, false},
                    { ProductVariationValueImagesSupported, false},
                    { VariationProductsSupported, false},
                    { VariationCombinationsSupported, false},
                    { SetArticlesSupported, false}
                };
            }
        }
    }
}
