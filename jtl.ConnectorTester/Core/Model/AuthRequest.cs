﻿using Newtonsoft.Json;

namespace jtl.ConnectorTester.Core.Model
{
    public class AuthRequest
    {
        [JsonProperty(Required = Required.Always, PropertyName = "token")]
        public string Token { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
