﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using jtl.ConnectorTester.Core.Model;
using jtl.ConnectorTester.Core.Rpc;
using jtl.ConnectorTester.Model;
using Newtonsoft.Json;
using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Reflection;
using System.Text;

namespace jtl.ConnectorTester.Core
{
    public class TrustAllCertificatePolicy : ICertificatePolicy
    {
        public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
        {
            return true;
        }
    }

    public class Connector
    {
        private readonly Subject<string> _jsonRequest = new Subject<string>();
        private readonly Subject<string> _jsonResponse = new Subject<string>();
        private readonly Subject<string> _jsonError = new Subject<string>();
        private readonly Subject<long> _calltimeMeasured = new Subject<long>();
        public IObservable<string> JsonRequest
        {
            get { return _jsonRequest.AsObservable(); }
        }
        public IObservable<string> JsonResponse
        {
            get { return _jsonResponse.AsObservable(); }
        }
        public IObservable<string> JsonError
        {
            get { return _jsonError.AsObservable(); }
        }
        public IObservable<long> CalltimeMeasured
        {
            get { return _calltimeMeasured.AsObservable(); }
        }
        public string SessionId { get; set; }
        public string TransactionId { get; set; }
        public byte[] Picture { get; set; }
        public string Picturename { get; set; }
        public List<Image> Images { get; set; }

        public Connector()
        {
            SessionId = string.Empty;
            Images = new List<Image>();
            ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
        }

        #region helper
        public RpcRequest BuildRequest(string method, object parameters)
        {
            var request = new RpcRequest
            {
                Id = Guid.NewGuid().ToString("N"),
                Jtlrpc = "2.0",
                Method = method,
                Params = parameters
            };

            return request;
        }

        public string BuildMethod(string model)
        {
            if (string.IsNullOrEmpty(model))
                return model;

            var newText = new StringBuilder(model.Length * 2);
            newText.Append(model[0]);
            for (var i = 1; i < model.Length; i++)
            {
                if (char.IsUpper(model[i]))
                    newText.Append('_');

                newText.Append(model[i]);
            }

            return newText.ToString().ToLower();
        }

        //protected RpcResponse Call(string url, string method, object parameters = null, bool withSession = false, NetworkCredential credentials = null)
        protected RpcResponse Call(string url, string method, object parameters = null, bool withSession = false)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var response = new RpcResponse();

            var http = new FastHttp();
            var request = BuildRequest(method, parameters);

            try
            {
                _jsonRequest.OnNext(request.ToString());

                if (withSession)
                {
                    var uriBuilder = new UriBuilder(url);
                    var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                    query["jtlauth"] = SessionId;
                    uriBuilder.Query = query.ToString();
                    url = uriBuilder.ToString();
                }

                http.Encoding = Encoding.UTF8;
                http.QueryParam.Add("jtlrpc", request.ToString());

                if (Picture != null)
                {
                    // Multiple images
                    if (Images.Count > 0)
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                            {
                                foreach (var image in Images)
                                {
                                    archive.CreateEntryFromFile(image.FilePath, string.Format("{0}_{1}{2}", image.HostId, image.RelationType, image.Extension));
                                }
                            }

                            http.UploadFile(url, "zip.zip", memoryStream.ToArray());
                        }
                    }
                    else
                    {
                        http.UploadFile(url, Picturename, Picture);   
                    }
                }
                else
                {
                    //http.Request(url, FastHttp.RequestMethod.Post, credentials);
                    http.Request(url, FastHttp.RequestMethod.Post);
                }

                stopWatch.Stop();

                if (http.StatusCode == HttpStatusCode.OK)
                {
                    response = JsonConvert.DeserializeObject<RpcResponse>(http.Content);
                    _jsonResponse.OnNext(response.ToString());
                    _calltimeMeasured.OnNext(stopWatch.ElapsedMilliseconds);
                }
            }
            catch (Exception exc)
            {
                _jsonError.OnNext(exc.Message);
                stopWatch.Stop();
                _calltimeMeasured.OnNext(stopWatch.ElapsedMilliseconds);
                throw new Exception(string.Format("Exception: {0} - Http Response: {1}", exc.Message, http.Content));
            }

            stopWatch.Reset();

            return response;
        }
        #endregion

        #region Calls

        public Task<RpcResponse> GetSessionAsync(string url, string token)
        {
            return Task.Run(() => GetSession(url, token));
        }

        public RpcResponse GetSession(string url, string token)
        {
            //return Call(url, "core.connector.auth", null, false, new NetworkCredential("jtl", token));
            return Call(url, "core.connector.auth", new AuthRequest { Token = token });
        }

        public Task<RpcResponse> PushListAsync(string url, string modelName, IEnumerable list)
        {
            return Task.Run(() => PushList(url, modelName, list));
        }

        public RpcResponse PushList(string url, string modelName, IEnumerable list)
        {
            return Call(url, string.Format("{0}.push", BuildMethod(modelName)), list, true);
        }

        public Task<RpcResponse> PushAsync(string url, string modelName, bool asList = true)
        {
            return Task.Run(() => PushIntern(url, modelName, null, asList));
        }

        public RpcResponse Push(string url, string modelName, bool asList = true)
        {
            return PushIntern(url, modelName, null, asList);
        }

        public Task<RpcResponse> PushAsync(string url, string modelName, MainModel model, bool asList = true)
        {
            return Task.Run(() => PushIntern(url, modelName, model, asList));
        }

        public RpcResponse Push(string url, string modelName, MainModel model, bool asList = true)
        {
            return PushIntern(url, modelName, model, asList);
        }

        protected RpcResponse PushIntern(string url, string modelName, MainModel model, bool asList = true)
        {
            if (model != null)
            {
                if (!asList)
                {
                    return Call(url, string.Format("{0}.push", BuildMethod(modelName)), model, true);
                }

                var list = new List<MainModel> {model};

                return Call(url, string.Format("{0}.push", BuildMethod(modelName)), list, true);
                //return Call(url, string.Format("{0}.push", BuildMethod(modelName)), model, true);
            }

            var obj =
                Assembly.GetExecutingAssembly()
                    .CreateInstance(string.Format("jtl.ConnectorTester.Model.{0}Model", modelName)) as MainModel;

            if (obj == null) return null;
            obj.Fill();

            return Call(url, string.Format("{0}.push", BuildMethod(modelName)), obj, true);
        }

        public Task<RpcResponse> PullAsync(string url, string model, ModelFilter filter)
        {
            return Task.Run(() => Pull(url, model, filter));
        }

        public RpcResponse Pull(string url, string model, ModelFilter filter)
        {
            return Call(url, string.Format("{0}.pull", BuildMethod(model)), filter, true);
        }

        public Task<RpcResponse> DeleteListAsync(string url, string modelName, IEnumerable list)
        {
            return Task.Run(() => DeleteList(url, modelName, list));
        }

        public RpcResponse DeleteList(string url, string modelName, IEnumerable list)
        {
            return Call(url, string.Format("{0}.delete", BuildMethod(modelName)), list, true);
        }

        public Task<RpcResponse> DeleteAsync(string url, string modelName)
        {
            return Task.Run(() => Delete(url, modelName));
        }

        public RpcResponse Delete(string url, string modelName)
        {
            return DeleteIntern(url, modelName, null);
        }

        public Task<RpcResponse> DeleteAsync(string url, string modelName, MainModel model, bool asList = true)
        {
            return Task.Run(() => Delete(url, modelName, model, asList));
        }

        public RpcResponse Delete(string url, string modelName, MainModel model, bool asList = true)
        {
            return DeleteIntern(url, modelName, model, asList);
        }

        protected RpcResponse DeleteIntern(string url, string modelName, MainModel model, bool asList = true)
        {
            if (model != null)
            {
                if (!asList)
                {
                    return Call(url, string.Format("{0}.delete", BuildMethod(modelName)), model, true);
                }

                var list = new List<MainModel> { model };

                return Call(url, string.Format("{0}.delete", BuildMethod(modelName)), list, true);
            }

            var obj =
                Assembly.GetExecutingAssembly()
                    .CreateInstance(string.Format("jtl.ConnectorTester.Model.{0}Model", modelName)) as MainModel;

            if (obj == null) return null;
            obj.Fill();

            return Call(url, string.Format("{0}.delete", BuildMethod(modelName)), obj, true);
        }

        public Task<RpcResponse> StatsAsync(string url, string modelName, ModelFilter filter)
        {
            return Task.Run(() => Stats(url, modelName, filter));
        }

        public RpcResponse Stats(string url, string model, ModelFilter filter)
        {
            return Call(url, string.Format("{0}.statistic", BuildMethod(model)), filter, true);
        }

        public Task<RpcResponse> SetStatusAsync(string url, CustomerOrderStatus status)
        {
            return Task.Run(() => SetStatus(url, status));
        }

        public RpcResponse SetStatus(string url, CustomerOrderStatus status)
        {
            return Call(url, "customer_order.set_status", status, true);
        }

        public Task<RpcResponse> SetDebugAsync(string url)
        {
            return Task.Run(() => SetDebug(url));
        }

        public RpcResponse SetDebug(string url)
        {
            return Call(url, "core.connector.debug", null, true);
        }

        public Task<RpcResponse> GetFeatureMatrixAsync(string url)
        {
            return Task.Run(() => GetFeatureMatrix(url));
        }

        public RpcResponse GetFeatureMatrix(string url)
        {
            return Call(url, "core.connector.features", null, true);
        }

        public Task<RpcResponse> GetLogsAsync(string url)
        {
            return Task.Run(() => GetLogs(url));
        }

        public RpcResponse GetLogs(string url)
        {
            return Call(url, "core.connector.logs", null, true);
        }

        public Task<RpcResponse> GetStatsAsync(string url, ModelFilter filter)
        {
            return Task.Run(() => GetStats(url, filter));
        }

        public RpcResponse GetStats(string url, ModelFilter filter)
        {
            return Call(url, "connector.statistic", filter, true);
        }

        public Task<RpcResponse> IdentifyAsync(string url)
        {
            return Task.Run(() => Identify(url));
        }

        public RpcResponse Identify(string url)
        {
            return Call(url, "connector.identify", null, true);
        }

        public Task<RpcResponse> FinishAsync(string url)
        {
            return Task.Run(() => Finish(url));
        }

        public RpcResponse Finish(string url)
        {
            return Call(url, "connector.finish", null, true);
        }

        public Task<RpcResponse> InitAsync(string url)
        {
            return Task.Run(() => Init(url));
        }

        public RpcResponse Init(string url)
        {
            return Call(url, "core.connector.init", null, true);
        }


        public Task<RpcResponse> ClearLinkerAsync(string url)
        {
            return Task.Run(() => ClearLinker(url));
        }

        public RpcResponse ClearLinker(string url)
        {
            return Call(url, "core.linker.clear", null, true);
        }

        public Task<RpcResponse> AckAsync(string url, AckModel ack)
        {
            return Task.Run(() => Ack(url, ack));
        }

        public RpcResponse Ack(string url, AckModel ack)
        {
            return Call(url, "core.connector.ack", ack, true);
        }

        public Task<RpcResponse> CommitAsync(string url, string model)
        {
            return Task.Run(() => Commit(url, model));
        }

        public RpcResponse Commit(string url, string model)
        {
            return Call(url, string.Format("{0}.commit", BuildMethod(model)), null, true);
        }

        public object ValidateContainer(string container, string json)
        {
            container = container.Replace("Model", "");
            var t = Type.GetType(String.Format("jtl.ConnectorTester.Model.Container.{0}", container));
            return JsonConvert.DeserializeObject(json, t);
        }

        public T ValidateContainerList<T>(string json)
        {
            return (T) Convert.ChangeType(JsonConvert.DeserializeObject<T>(json), typeof(T));
        }

        public object ValidateModel(string model, string json)
        {
            var t = Type.GetType(String.Format("jtl.ConnectorTester.Model.{0}Model", model));
            return model == "Identity" ? JsonConvert.DeserializeObject(json, t, new IdentityConverter()) : JsonConvert.DeserializeObject(json, t);
        }

        #endregion
    }
}